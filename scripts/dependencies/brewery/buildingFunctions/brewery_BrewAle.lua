local rabenhugelFarm = ...

-- Setup the BREW ALE
--https://www.polymorph.games/foundation/modding/api/building_function_workplace

rabenhugelFarm:register({
    DataType = "BUILDING_FUNCTION_WORKPLACE",
    Id = "RAVH_ASSIGNABLE_BREW_ALE_FUNCTION",

    Name = "RAVH_ASSIGNABLE_BREW_ALE_FUNCTION_NAME",
-- BUILDING_FUNCTION_WORKPLACE Properties
    WorkerCapacity = 2,
    --UpkeepPerWorker
    RelatedJob = { Job = "BREWER", Behavior = "WORK_BEHAVIOR" },
    --IsPrivate
    --HasResourceDepot = true,
    InputInventoryCapacity = { 
        -- how much of a resource a building can actually hold
            { Resource = "RAVH_SACK_GRAIN_WHEAT", Quantity = 20 },
            { Resource = "HOP", Quantity = 10 },
            { Resource = "BARREL", Quantity = 10 },
            { Resource = "WATER", Quantity = 10 }
        },
        ResourceListNeeded = { 
            { Resource = "RAVH_SACK_GRAIN_WHEAT", Quantity = 2 },
            { Resource = "HOP", Quantity = 1 },
            { Resource = "BARREL", Quantity = 1 },
            { Resource = "WATER", Quantity = 1 }
        },
        ResourceProduced = { 
            { Resource = "RAVH_ALE", Quantity = 6}
        },
    -- DesirabilityLayer
})