local rabenhugelFarm = ...

-- Register Rabenhugel Farm Function List
rabenhugelFarm:register({
    DataType = "BUILDING_FUNCTION_LIST",
    Id = "RAVH_BREWERY_FUNCTION_LIST",
    AssetFunctionList = {
            "RAVH_ASSIGNABLE_BREW_BEER_FUNCTION",
            "RAVH_ASSIGNABLE_BREW_ALE_FUNCTION"
    }
})

rabenhugelFarm:register({
    DataType = "BUILDING_FUNCTION_ASSIGNABLE",
    Id = "RAVH_BREWERY_ASSIGNABLE_FUNCTION_LIST",
    AuthorizedFunctionList = "RAVH_BREWERY_FUNCTION_LIST"
})

-- Override Bakery Workplace Function
rabenhugelFarm:override({
    DataType = "BUILDING_PART",
    Id = "BUILDING_PART_BREWERY",
    AssetBuildingFunction = "RAVH_BREWERY_ASSIGNABLE_FUNCTION_LIST"
})