local rabenhugelFarm = ...

-- Setup the BARLEY Milling FUNCTIONALITY
--https://www.polymorph.games/foundation/modding/api/building_function_workplace

rabenhugelFarm:register({
    DataType = "BUILDING_FUNCTION_WORKPLACE",
    Id = "RAVH_ASSIGNABLE_BAKE_RYE_BREAD_FUNCTION",

    Name = "RAVH_ASSIGNABLE_BAKE_RYE_BREAD_FUNCTION_NAME",
-- BUILDING_FUNCTION_WORKPLACE Properties
    WorkerCapacity = 2,
    --UpkeepPerWorker
    RelatedJob = { Job = "BAKER", Behavior = "WORK_BEHAVIOR" },
    --IsPrivate
    --HasResourceDepot = true,
    InputInventoryCapacity = { 
        -- how much of a resource a building can actually hold
            { Resource = "RAVH_SACK_FLOUR_RYE", Quantity = 10 },
            { Resource = "WATER", Quantity = 5 }
        },
        ResourceListNeeded = { 
            { Resource = "RAVH_SACK_FLOUR_RYE", Quantity = 1 },
            { Resource = "WATER", Quantity = 1 }
        },
        ResourceProduced = { 
            { Resource = "RAVH_BREAD_RYE", Quantity = 6}
        },
    -- DesirabilityLayer
})