local rabenhugelFarm = ...

-- Register Rabenhugel Farm Function List
rabenhugelFarm:register({
    DataType = "BUILDING_FUNCTION_LIST",
    Id = "RAVH_BAKERY_FUNCTION_LIST",
    AssetFunctionList = {
            "RAVH_ASSIGNABLE_BAKE_BARLEY_BREAD_FUNCTION",
            "RAVH_ASSIGNABLE_BAKE_RYE_BREAD_FUNCTION",
            "RAVH_ASSIGNABLE_BAKE_WHEAT_BREAD_FUNCTION"
    }
})

rabenhugelFarm:register({
    DataType = "BUILDING_FUNCTION_ASSIGNABLE",
    Id = "RAVH_BAKERY_ASSIGNABLE_FUNCTION_LIST",
    AuthorizedFunctionList = "RAVH_BAKERY_FUNCTION_LIST"
})

-- Override Bakery Workplace Function
rabenhugelFarm:override({
    DataType = "BUILDING_PART",
    Id = "BUILDING_PART_BAKERY",
    AssetBuildingFunction = "RAVH_BAKERY_ASSIGNABLE_FUNCTION_LIST"
})