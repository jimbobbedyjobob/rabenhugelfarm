local rabenhugelFarm = ...

rabenhugelFarm:override({
    DataType = "BUILDING_FUNCTION_LIST",
    Id = "POST_MILL_ASSIGNABLE_FUNCTION_LIST",
    AssetFunctionList = {
            Action = "APPEND",
            "RAVH_BUILDING_FUNCTION_MILLING_BARLEY",
            "RAVH_BUILDING_FUNCTION_MILLING_OATS",
            "RAVH_BUILDING_FUNCTION_MILLING_RYE",
            "RAVH_BUILDING_FUNCTION_MILLING_WHEAT"
    }
})