local rabenhugelFarm = ...

-- Setup the BARLEY Milling FUNCTIONALITY
--https://www.polymorph.games/foundation/modding/api/building_function_workplace

rabenhugelFarm:register({
    DataType = "BUILDING_FUNCTION_WORKPLACE",
    Id = "RAVH_BUILDING_FUNCTION_MILLING_BARLEY",

    Name = "RAVH_ASSIGNABLE_MILLING_BARLEY_FUNCTION_NAME",
-- BUILDING_FUNCTION_WORKPLACE Properties
    WorkerCapacity = 2,
    --UpkeepPerWorker
    RelatedJob = { Job = "MILLER", Behavior = "WORK_BEHAVIOR" },
    --IsPrivate
    --HasResourceDepot = true,
    InputInventoryCapacity = { 
        -- how much of a resource a building can actually hold
            { Resource = "RAVH_SACK_GRAIN_BARLEY", Quantity = 30 },
        },
        ResourceListNeeded = { 
            { Resource = "RAVH_SACK_GRAIN_BARLEY", Quantity = 2 },
        },
        ResourceProduced = { 
            { Resource = "RAVH_SACK_FLOUR_BARLEY", Quantity = 1}
        },
    -- DesirabilityLayer
})