local rabenhugelFarm = ...

-- Setup the OAT Milling FUNCTIONALITY
--https://www.polymorph.games/foundation/modding/api/building_function_workplace

rabenhugelFarm:register({
    DataType = "BUILDING_FUNCTION_WORKPLACE",
    Id = "RAVH_BUILDING_FUNCTION_MILLING_OATS",

    Name = "RAVH_ASSIGNABLE_MILLING_OATS_FUNCTION_NAME",
-- BUILDING_FUNCTION_WORKPLACE Properties
    WorkerCapacity = 2,
    --UpkeepPerWorker
    RelatedJob = { Job = "MILLER", Behavior = "WORK_BEHAVIOR" },
    --IsPrivate
    --HasResourceDepot = true,
    InputInventoryCapacity = { 
        -- how much of a resource a building can actually hold
            { Resource = "RAVH_SACK_GRAIN_OAT", Quantity = 30 },
        },
        ResourceListNeeded = { 
            { Resource = "RAVH_SACK_GRAIN_OAT", Quantity = 2 },
        },
        ResourceProduced = { 
            { Resource = "RAVH_SACK_OATS", Quantity = 1}
        },
    -- DesirabilityLayer
})