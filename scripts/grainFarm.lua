local rabenhugelFarm = ...

-- Register the Grain Farm in inverse order of dependency

-- Register Abstract Assets
rabenhugelFarm:dofile("scripts/grainFarm/abstractAssets/zones.lua")

-- Register Grain Farm Resources in sequence
rabenhugelFarm:dofile("scripts/grainFarm/baseResources/resourcesPlants.lua")
rabenhugelFarm:dofile("scripts/grainFarm/baseResources/resourcesBushels.lua")
rabenhugelFarm:dofile("scripts/grainFarm/baseResources/resourcesGrains.lua")
rabenhugelFarm:dofile("scripts/grainFarm/baseResources/resourcesFlour.lua")

rabenhugelFarm:dofile("scripts/grainFarm/supplementaryResources/resourcesHaystack.lua")
rabenhugelFarm:dofile("scripts/grainFarm/supplementaryResources/resourcesStraw.lua")

    -- Register Higher Level Resources
    rabenhugelFarm:dofile("scripts/higherLevelResources/resourcesBreads.lua")
    rabenhugelFarm:dofile("scripts/higherLevelResources/resourcesAle.lua")


-- Register Field Types
        -- Register Field Configurations
        rabenhugelFarm:dofile("scripts/grainFarm/fieldConfigurations/barleyFieldConfig.lua")
        rabenhugelFarm:dofile("scripts/grainFarm/fieldConfigurations/oatFieldConfig.lua")
        rabenhugelFarm:dofile("scripts/grainFarm/fieldConfigurations/ryeFieldConfig.lua")
        rabenhugelFarm:dofile("scripts/grainFarm/fieldConfigurations/wheatFieldConfig.lua")

-- Register Jobs & Related
    -- Tools
            --grainFarmerScythe = rabenhugelFarm:dofile("scripts/grainFarm/tools/registerScythe.lua")
    -- Character SetUp
    -- cannot Register Character Setups so tables are built in separate scripts for Order and Ease
    -- create containers for the data to pass to the Agents and Functions
            -- Using Vanilla Farmer ATM
        genericFarmhand = rabenhugelFarm:dofile("scripts/grainFarm/jobs/charactersAndAgents/farmhand_Generic_CHAR.lua")
        farmhandSowing = rabenhugelFarm:dofile("scripts/grainFarm/jobs/charactersAndAgents/farmhandSowing_CHAR.lua")
        farmhandHarvesting = rabenhugelFarm:dofile("scripts/grainFarm/jobs/charactersAndAgents/farmhandHarvesting_CHAR.lua")
        grainThresher = rabenhugelFarm:dofile("scripts/grainFarm/jobs/charactersAndAgents/grainThresher_CHAR.lua")
        tennantFarmer = rabenhugelFarm:dofile("scripts/grainFarm/jobs/charactersAndAgents/tennantFarmer_CHAR.lua")
        rabenhugelFarm:log ("Character Setup Containters Created")

    -- Agent Set Up - Required for Farm Functions
    -- I guess Agent Setups like this are for multi-work jobs, like Farmer with Sowing or Harvesting
    rabenhugelFarm:dofile("scripts/grainFarm/jobs/charactersAndAgents/agentSetup.lua")

    -- Jobs
        rabenhugelFarm:dofile("scripts/grainFarm/jobs/farmhand.lua")
        rabenhugelFarm:dofile("scripts/grainFarm/jobs/thresherAndWinnower.lua")
        rabenhugelFarm:dofile("scripts/grainFarm/jobs/tennantFarmer.lua")

-- Register Building Functions
rabenhugelFarm:dofile("scripts/grainFarm/buildingFunctions/seedHut_GrowBarley_function.lua")
rabenhugelFarm:dofile("scripts/grainFarm/buildingFunctions/seedHut_GrowOats_function.lua")
rabenhugelFarm:dofile("scripts/grainFarm/buildingFunctions/seedHut_GrowRye_function.lua")
rabenhugelFarm:dofile("scripts/grainFarm/buildingFunctions/seedHut_GrowWheat_function.lua")

rabenhugelFarm:dofile("scripts/grainFarm/buildingFunctions/threshingFloor_ThreshBarley_function.lua")
rabenhugelFarm:dofile("scripts/grainFarm/buildingFunctions/threshingFloor_ThreshOats_function.lua")
rabenhugelFarm:dofile("scripts/grainFarm/buildingFunctions/threshingFloor_ThreshRye_function.lua")
rabenhugelFarm:dofile("scripts/grainFarm/buildingFunctions/threshingFloor_ThreshWheat_function.lua")

--rabenhugelFarm:dofile("scripts/grainFarm/buildingFunctions/haystack.lua")
rabenhugelFarm:dofile("scripts/grainFarm/buildingFunctions/grainSilo_function.lua")

-- Register Buildings
rabenhugelFarm:dofile("scripts/grainFarm/buildings/seedHut/setupSeedHut.lua")
--rabenhugelFarm:dofile("scripts/grainFarm/buildings/setupHaystack.lua")
rabenhugelFarm:dofile("scripts/grainFarm/buildings/threshingFloor/setupThreshingFloor.lua")
rabenhugelFarm:dofile("scripts/grainFarm/buildings/grainSilo/setupSiloScaleable.lua")

-- Register Dependencies
    -- Post Mill
    -- Register Building Functions
    rabenhugelFarm:dofile("scripts/dependencies/postMill/buildingFunctions/postMill_MillBarley.lua")
    rabenhugelFarm:dofile("scripts/dependencies/postMill/buildingFunctions/postMill_MillOats.lua")
    rabenhugelFarm:dofile("scripts/dependencies/postMill/buildingFunctions/postMill_MillRye.lua")
    rabenhugelFarm:dofile("scripts/dependencies/postMill/buildingFunctions/postMill_MillWheat.lua")

    -- Append Functions to Dependencies
    --rabenhugelFarm:dofile("scripts/dependencies/postMill/appendMillFunctions.lua")

    -- Vanilla Bakery
    -- Register Building Functions
    rabenhugelFarm:dofile("scripts/dependencies/bakery/buildingFunctions/bakery_BakeBarleyBread.lua")
    rabenhugelFarm:dofile("scripts/dependencies/bakery/buildingFunctions/bakery_BakeRyeBread.lua")
    rabenhugelFarm:dofile("scripts/dependencies/bakery/buildingFunctions/bakery_BakeWheatBread.lua")

    -- Append Functions to Dependencies
    rabenhugelFarm:dofile("scripts/dependencies/bakery/appendBakeryFunctions.lua")

    -- Vanilla Brewery
    -- Register Building Functions
    rabenhugelFarm:dofile("scripts/dependencies/brewery/buildingFunctions/brewery_BrewBeer.lua")
    rabenhugelFarm:dofile("scripts/dependencies/brewery/buildingFunctions/brewery_BrewAle.lua")
    
    -- Append Functions to Dependencies
    rabenhugelFarm:dofile("scripts/dependencies/brewery/appendBreweryFunctions.lua")



rabenhugelFarm:log("Grain Farm Registry Complete")