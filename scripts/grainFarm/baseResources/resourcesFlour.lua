local rabenhugelFarm = ...

-- Register all meshes, icons and resources for Grain FLOUR

-- Register MODELS
rabenhugelFarm:registerAssetId("models/grainFarm/resources/flourSackBarley.fbx/Prefab/FlourSack","PREFAB_RAVH_FLOUR_SACK_BARLEY")
rabenhugelFarm:registerAssetId("models/grainFarm/resources/sackOats.fbx/Prefab/FlourSack","PREFAB_RAVH_SACK_OATS")
rabenhugelFarm:registerAssetId("models/grainFarm/resources/flourSackRye.fbx/Prefab/FlourSack","PREFAB_RAVH_FLOUR_SACK_RYE")
rabenhugelFarm:registerAssetId("models/grainFarm/resources/flourSackWheat.fbx/Prefab/FlourSack","PREFAB_RAVH_FLOUR_SACK_WHEAT")

-- Register ICONS
rabenhugelFarm:registerAssetId("icons/grainFarm/sack_flour_barley_icon.png", "RAVH_SACK_FLOUR_BARLEY_ICON")
rabenhugelFarm:registerAssetId("icons/grainFarm/sack_flour_oat_icon.png", "RAVH_SACK_OATS_ICON")
rabenhugelFarm:registerAssetId("icons/grainFarm/sack_flour_rye_icon.png", "RAVH_SACK_FLOUR_RYE_ICON")
rabenhugelFarm:registerAssetId("icons/grainFarm/sack_flour_wheat_icon.png", "RAVH_SACK_FLOUR_WHEAT_ICON")

-- Register RESOURCES
-- SACKS of FLOUR
rabenhugelFarm:register({
	DataType = "RESOURCE",
	Id = "RAVH_SACK_FLOUR_BARLEY",
	ResourceName = "RAVH_SACK_BARLEY_FLOUR_NAME",
	Icon = "RAVH_SACK_FLOUR_BARLEY_ICON",
	ResourceTypeList = { "RESOURCE_FOOD" }, -- Can be stored in Granary but cannot be sold at Market Stalls and cannot fulfill Food Needs
	IsTradable = true,
	ResourceLayoutType = "BAGS",
	Prefab = "PREFAB_RAVH_FLOUR_SACK_BARLEY",
	DisplayInToolbar = true
})

rabenhugelFarm:register({
	DataType = "RESOURCE",
	Id = "RAVH_SACK_OATS",
	ResourceName = "RAVH_SACK_OATS_NAME",
	Icon = "RAVH_SACK_OATS_ICON",
	ResourceTypeList = { "RESOURCE_FOOD" }, -- Can be stored in Granary but cannot be sold at Market Stalls and cannot fulfill Food Needs
	IsTradable = true,
	ResourceLayoutType = "BAGS",
	Prefab = "PREFAB_RAVH_SACK_OATS",
	DisplayInToolbar = true
})

rabenhugelFarm:register({
	DataType = "RESOURCE",
	Id = "RAVH_SACK_FLOUR_RYE",
	ResourceName = "RAVH_SACK_RYE_FLOUR_NAME",
	Icon = "RAVH_SACK_FLOUR_RYE_ICON",
	ResourceTypeList = { "RESOURCE_FOOD" }, -- Can be stored in Granary but cannot be sold at Market Stalls and cannot fulfill Food Needs
	IsTradable = true,
	ResourceLayoutType = "BAGS",
	Prefab = "PREFAB_RAVH_FLOUR_SACK_RYE",
	DisplayInToolbar = true
})

rabenhugelFarm:register({
	DataType = "RESOURCE",
	Id = "RAVH_SACK_FLOUR_WHEAT",
	ResourceName = "RAVH_SACK_WHEAT_FLOUR_NAME",
	Icon = "RAVH_SACK_FLOUR_WHEAT_ICON",
	ResourceTypeList = { "RESOURCE_FOOD" }, -- Can be stored in Granary but cannot be sold at Market Stalls and cannot fulfill Food Needs
	IsTradable = true,
	ResourceLayoutType = "BAGS",
	Prefab = "PREFAB_RAVH_FLOUR_SACK_WHEAT",
	DisplayInToolbar = true
})