local rabenhugelFarm = ...

-- Register all meshes, icons and resources for Grain SACKS

-- Register MODELS
--rabenhugelFarm:registerAssetId("models/grainFarm/defaultGrainSack.fbx/Prefab/GrainSack","RAVH_DEFAULT_GRAIN_SACK")
rabenhugelFarm:registerAssetId("models/grainFarm/resources/grainSackBarley.fbx/Prefab/GrainSack","PREFAB_RAVH_GRAINSACK_BARLEY")
rabenhugelFarm:registerAssetId("models/grainFarm/resources/grainSackOat.fbx/Prefab/GrainSack","PREFAB_RAVH_GRAINSACK_OAT")
rabenhugelFarm:registerAssetId("models/grainFarm/resources/grainSackRye.fbx/Prefab/GrainSack","PREFAB_RAVH_GRAINSACK_RYE")
rabenhugelFarm:registerAssetId("models/grainFarm/resources/grainSackWheat.fbx/Prefab/GrainSack","PREFAB_RAVH_GRAINSACK_WHEAT")

-- Register ICONS
rabenhugelFarm:registerAssetId("icons/grainFarm/sack_grain_barley_icon.png", "RAVH_SACK_GRAIN_BARLEY_ICON")
rabenhugelFarm:registerAssetId("icons/grainFarm/sack_grain_oat_icon.png", "RAVH_SACK_GRAIN_OAT_ICON")
rabenhugelFarm:registerAssetId("icons/grainFarm/sack_grain_rye_icon.png", "RAVH_SACK_GRAIN_RYE_ICON")
rabenhugelFarm:registerAssetId("icons/grainFarm/sack_grain_wheat_icon.png", "RAVH_SACK_GRAIN_WHEAT_ICON")

-- Register RESOURCES
-- SACKS of GRAIN
rabenhugelFarm:register({
	DataType = "RESOURCE",
	Id = "RAVH_SACK_GRAIN_BARLEY",
	ResourceName = "RAVH_SACK_BARLEY_GRAIN_NAME",
	Icon = "RAVH_SACK_GRAIN_BARLEY_ICON",
	ResourceTypeList = { "FOOD" }, 
	IsTradable = true,
	SellingPrice = 1.0,
	BuyingPrice = 1.2,
	VillagerBuyingPrice =     
	{
        Resource = "GOLD",
        Quantity = 5
    },
	ResourceLayoutType = "BAGS",
	Prefab = "PREFAB_RAVH_GRAINSACK_BARLEY",
	DisplayInToolbar = true
})

rabenhugelFarm:register({
	DataType = "RESOURCE",
	Id = "RAVH_SACK_GRAIN_OAT",
	ResourceName = "RAVH_SACK_OAT_GRAIN_NAME",
	Icon = "RAVH_SACK_GRAIN_OAT_ICON",
	ResourceTypeList = { "FOOD" }, 
	IsTradable = true,
	SellingPrice = 1.2,
	BuyingPrice = 1.4,
	VillagerBuyingPrice =     
	{
        Resource = "GOLD",
        Quantity = 8
    },
	ResourceLayoutType = "BAGS",
	Prefab = "PREFAB_RAVH_GRAINSACK_OAT",
	DisplayInToolbar = true
})

rabenhugelFarm:register({
	DataType = "RESOURCE",
	Id = "RAVH_SACK_GRAIN_RYE",
	ResourceName = "RAVH_SACK_RYE_GRAIN_NAME",
	Icon = "RAVH_SACK_GRAIN_RYE_ICON",
	ResourceTypeList = { "FOOD" },
	IsTradable = true,
	SellingPrice = 1.4,
	BuyingPrice = 1.8,
	VillagerBuyingPrice =     
	{
        Resource = "GOLD",
        Quantity = 10
    },
	ResourceLayoutType = "BAGS",
	Prefab = "PREFAB_RAVH_GRAINSACK_RYE",
	DisplayInToolbar = true
})

rabenhugelFarm:register({
	DataType = "RESOURCE",
	Id = "RAVH_SACK_GRAIN_WHEAT",
	ResourceName = "RAVH_SACK_WHEAT_GRAIN_NAME",
	Icon = "RAVH_SACK_GRAIN_WHEAT_ICON",
	ResourceTypeList = { "FOOD" }, 
	IsTradable = true,
	SellingPrice = 1.8,
	BuyingPrice = 2.2,
	VillagerBuyingPrice =     
	{
        Resource = "GOLD",
        Quantity = 12
    },
	ResourceLayoutType = "BAGS",
	Prefab = "PREFAB_RAVH_GRAINSACK_WHEAT",
	DisplayInToolbar = true
})


