local rabenhugelFarm = ...

-- Register all meshes, icons and resources for Grain PLANTS

-- Register MODELS
rabenhugelFarm:registerAssetId("models/grainFarm/resources/plantBarley.fbx/Prefab/GrainPlant","PREFAB_RAVH_BARLEY_PLANT")
rabenhugelFarm:registerAssetId("models/grainFarm/resources/plantOat.fbx/Prefab/GrainPlant","PREFAB_RAVH_OAT_PLANT")
rabenhugelFarm:registerAssetId("models/grainFarm/resources/plantRye.fbx/Prefab/GrainPlant","PREFAB_RAVH_RYE_PLANT")
rabenhugelFarm:registerAssetId("models/grainFarm/resources/plantWheat.fbx/Prefab/GrainPlant","PREFAB_RAVH_WHEAT_PLANT")

rabenhugelFarm:log("Plant Prefabs Registered")

-- Register ICONS
--rabenhugelFarm:registerAssetId("icons/grainFarm/plant_barley_icon.png", "RAVH_PLANT_BARLEY_ICON")
--rabenhugelFarm:registerAssetId("icons/grainFarm/plant_oat_icon.png", "RAVH_PLANT_OAT_ICON")
--rabenhugelFarm:registerAssetId("icons/grainFarm/plant_rye_icon.png", "RAVH_PLANT_RYE_ICON")
--rabenhugelFarm:registerAssetId("icons/grainFarm/plant_wheat_icon.png", "RAVH_PLANT_WHEAT_ICON")

-- Register Plantable Component
-- Cannot register PLANTABLE as COMPONENT because it isn'T a component

rabenhugelFarm:register({
	DataType = "PLANTABLE",
	Id = "RAVH_PLANT_BARLEY",

    Name = "RAVH_PLANT_BARLEY_NAME",
    PlantPrefab = "PREFAB_RAVH_BARLEY_PLANT",
    MinimumScale = 0.0,
    MaximumScale = 1.0
})
rabenhugelFarm:register({
	DataType = "PLANTABLE",
	Id = "RAVH_PLANT_OAT",
	
    Name = "RAVH_PLANT_OAT_NAME",
    PlantPrefab = "PREFAB_RAVH_OAT_PLANT",
    MinimumScale = 0.0,
    MaximumScale = 1.0
})
rabenhugelFarm:register({
	DataType = "PLANTABLE",
	Id = "RAVH_PLANT_RYE",
		
    Name = "RAVH_PLANT_RYE_NAME",
    PlantPrefab = "PREFAB_RAVH_RYE_PLANT",
    MinimumScale = 0.0,
    MaximumScale = 1.0
})
rabenhugelFarm:register({
	DataType = "PLANTABLE",
	Id = "RAVH_PLANT_WHEAT",
		
    Name = "RAVH_PLANT_WHEAT_NAME",
    PlantPrefab = "PREFAB_RAVH_WHEAT_PLANT",
    MinimumScale = 0.0,
    MaximumScale = 1.0
})

rabenhugelFarm:log("Plantables Registered")