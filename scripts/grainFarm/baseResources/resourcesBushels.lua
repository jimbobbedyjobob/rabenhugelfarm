local rabenhugelFarm = ...

-- Register all meshes, icons and resources for Grain BUSHELS

-- Register MODELS
--rabenhugelFarm:registerAssetId("models/grainFarm/defaultBushel.fbx/Prefab/Bushel","RAVH_DEFAULT_BUSHEL")
rabenhugelFarm:registerAssetId("models/grainFarm/resources/bushelBarley.fbx/Prefab/Bushel","PREFAB_RAVH_BUSHEL_BARLEY")
rabenhugelFarm:registerAssetId("models/grainFarm/resources/bushelOat.fbx/Prefab/Bushel","PREFAB_RAVH_BUSHEL_OAT")
rabenhugelFarm:registerAssetId("models/grainFarm/resources/bushelRye.fbx/Prefab/Bushel","PREFAB_RAVH_BUSHEL_RYE")
rabenhugelFarm:registerAssetId("models/grainFarm/resources/bushelWheat.fbx/Prefab/Bushel","PREFAB_RAVH_BUSHEL_WHEAT")

-- Register ICONS
rabenhugelFarm:registerAssetId("icons/grainFarm/bushel_barley_icon.png", "RAVH_BUSHEL_BARLEY_ICON")
rabenhugelFarm:registerAssetId("icons/grainFarm/bushel_oat_icon.png", "RAVH_BUSHEL_OAT_ICON")
rabenhugelFarm:registerAssetId("icons/grainFarm/bushel_rye_icon.png", "RAVH_BUSHEL_RYE_ICON")
rabenhugelFarm:registerAssetId("icons/grainFarm/bushel_wheat_icon.png", "RAVH_BUSHEL_WHEAT_ICON")

-- Register RESOURCES
-- BUSHELS - I hope these will appear, eventually, every time a farmer does a sweep witha scythe
-- https://www.polymorph.games/foundation/modding/api/resource
rabenhugelFarm:register({
	DataType = "RESOURCE",
	Id = "RAVH_BUSHEL_BARLEY",
	ResourceName = "RAVH_BUSHEL_BARLEY_NAME",
	Icon = "RAVH_BUSHEL_BARLEY_ICON",
	ResourceTypeList = { "RESOURCE_FOOD" }, -- Can be stored in Granary but cannot be sold at Market Stalls and cannot fulfill Food Needs
	IsTradable = true,
	ResourceLayoutType = "BAGS",
	Prefab = "PREFAB_RAVH_BUSHEL_BARLEY",
	DisplayInToolbar = false
})

rabenhugelFarm:register({
	DataType = "RESOURCE",
	Id = "RAVH_BUSHEL_OAT",
	ResourceName = "RAVH_BUSHEL_OAT_NAME",
	Icon = "RAVH_BUSHEL_OAT_ICON",
	ResourceTypeList = { "RESOURCE_FOOD" }, -- Can be stored in Granary but cannot be sold at Market Stalls and cannot fulfill Food Needs
	IsTradable = true,
	ResourceLayoutType = "BAGS",
	Prefab = "PREFAB_RAVH_BUSHEL_OAT",
	DisplayInToolbar = false
})

rabenhugelFarm:register({
	DataType = "RESOURCE",
	Id = "RAVH_BUSHEL_RYE",
	ResourceName = "RAVH_BUSHEL_RYE_NAME",
	Icon = "RAVH_BUSHEL_RYE_ICON",
	ResourceTypeList = { "RESOURCE_FOOD" }, -- Can be stored in Granary but cannot be sold at Market Stalls and cannot fulfill Food Needs
	IsTradable = true,
	ResourceLayoutType = "BAGS",
	Prefab = "PREFAB_RAVH_BUSHEL_RYE",
	DisplayInToolbar = false
})

rabenhugelFarm:register({
	DataType = "RESOURCE",
	Id = "RAVH_BUSHEL_WHEAT",
	ResourceName = "RAVH_BUSHEL_WHEAT_NAME",
	Icon = "RAVH_BUSHEL_WHEAT_ICON",
	ResourceTypeList = { "RESOURCE_FOOD" }, -- Can be stored in Granary but cannot be sold at Market Stalls and cannot fulfill Food Needs
	IsTradable = true,
	ResourceLayoutType = "BAGS",
	Prefab = "PREFAB_RAVH_BUSHEL_WHEAT",
	DisplayInToolbar = false
})