local rabenhugelFarm = ...

-- Register all ZONES required for the Grain Farm mod
-- https://www.polymorph.games/foundation/modding/api/zone

-- Register Zones
rabenhugelFarm:register({
    DataType = "ZONE",
    Id = "BARELY_FIELD_ZONE",

    Name = "RAVH_FIELD_BARLEY_NAME",

    Color = {0.933, 0.733, 0.455, 1.0},
    Description = "RAVH_BARLEY_FIELD_DESCRIPTION"
})

rabenhugelFarm:register({
    DataType = "ZONE",
    Id = "OAT_FIELD_ZONE",

    Name = "RAVH_FIELD_OAT_NAME",

    Color = {0.49, 0.337, 0.107, 1.0},
    Description = "RAVH_OAT_FIELD_DESCRIPTION"
})

rabenhugelFarm:register({
    DataType = "ZONE",
    Id = "RYE_FIELD_ZONE",

    Name = "RAVH_FIELD_RYE_NAME",

    Color = {0.208, 0.314, 0.086, 1.0},
    Description = "RAVH_RYE_FIELD_DESCRIPTION"
})

rabenhugelFarm:register({
    DataType = "ZONE",
    Id = "WHEAT_FIELD_ZONE",

    Name = "RAVH_FIELD_WHEAT_NAME",

    Color = {0.851, 0.424, 0.047, 1.0},
    Description = "RAVH_WHEAT_FIELD_DESCRIPTION"
})

rabenhugelFarm:log("Zones Registered")