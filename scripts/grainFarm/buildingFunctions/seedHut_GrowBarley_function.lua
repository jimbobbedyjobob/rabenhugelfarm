local rabenhugelFarm = ...

-- Setup the Grow Functionality of the Seed Hut for BARLEY
rabenhugelFarm:register({
	    -- BUILDING Properties
-- https://www.polymorph.games/foundation/modding/api/building
	DataType = "BUILDING_FUNCTION_FARM",
	Id = "RAVH_BUILDING_FUNCTION_SEEDHUT_BARLEY",

	Name = "RAVH_ASSIGNABLE_BARLEY_GROWING_FUNCTION_NAME",
	-- BUILDING_FUNCTION_WORKPLACE Properties
	--https://www.polymorph.games/foundation/modding/api/building_function_workplace
	WorkerCapacity = 3,
	--UpkeepPerWorker
	RelatedJob = { Job = "RAVH_FARMHAND", Behavior = "BEHAVIOR_WORK" },
    --IsPrivate
    HasResourceDepot = true,
	InputInventoryCapacity = { 
    -- how much of a resource a building can actually hold coming in
        --{ Resource = "RAVH_SACK_GRAIN_BARLEY", Quantity = 1 }, -- Causes Circular Calculations in Weekly Production Calcs
		{ Resource = "WATER", Quantity = 10 },
		{ Resource = "TOOL", Quantity = 10 }
	},
	ResourceListNeeded = { 
    -- required to do work, may not apply because only certain jobs require resources in their vanilla behaviour
        --{ Resource = "RAVH_SACK_GRAIN_BARLEY", Quantity = 1 },  -- Causes Circular Calculations in Weekly Production Calcs
		{ Resource = "WATER", Quantity = 1 },
		{ Resource = "TOOL", Quantity = 1 }
    },
    ResourceProduced = { 
    -- used until the Farming Behaviour is opened up and it can work like a real farm
        { Resource = "RAVH_BUSHEL_BARLEY", Quantity = 80}
    },
	-- BUILDING_FUNCTION_FARM Properties
	-- https://www.polymorph.games/foundation/modding/api/building_function_farm
	SowingDurationPerc = {0, 20},
	GrowDuration = {20, 21},
	GrowWaitDuration = {20, 22},
	HarvestDuration = {22, 23},
	RestDuration = {23, 24},
	QuantityToHarvestPerStep = 30,
	HarvestStepsCount = 5,
	QuantityToPlantPerStep = 30,
	PlantingStepsCount = 5,
	ResourceValuePerGather = 1,
	CropFieldConfig = "RAVH_BARLEY_FIELDCONFIG",
	HarvestSetup= "RAVH_CHARACTER_GRAIN_HARVESTING_FARMER",
	SowingSetup = "RAVH_CHARACTER_GRAIN_SOWING_FARMER"
})