local rabenhugelFarm = ...

-- Setup the THRESHING BARLEY FUNCTIONALITY of the Threshing Floor
--https://www.polymorph.games/foundation/modding/api/building_function_workplace

rabenhugelFarm:register({
    DataType = "BUILDING_FUNCTION_WORKPLACE",
    Id = "RAVH_BUILDING_FUNCTION_THRESHINGFLOOR_BARLEY",

    Name = "RAVH_ASSIGNABLE_THRESHING_BARLEY_FUNCTION_NAME",
-- BUILDING_FUNCTION_WORKPLACE Properties
    WorkerCapacity = 8,
    --UpkeepPerWorker
    RelatedJob = { Job = "RAVH_THRESHERWINNOWER", Behavior = "BEHAVIOR_WORK" },
    --IsPrivate
    HasResourceDepot = true,
    InputInventoryCapacity = { 
        -- how much of a resource a building can actually hold
            { Resource = "RAVH_BUSHEL_BARLEY", Quantity = 100 },
        },
        ResourceListNeeded = { 
        -- required to do work, may not apply because only certain jobs require resources in their vanilla behaviour
            { Resource = "RAVH_BUSHEL_BARLEY", Quantity = 10 },
        },
        ResourceProduced = { 
        -- used until the Farming Behaviour is opened up and it can work like a real farm
            { Resource = "RAVH_SACK_GRAIN_BARLEY", Quantity = 1}
            --{ Resource = "RAHV_BALE_BARLEY_STRAW", Quantity = 10}
        },
    -- DesirabilityLayer

})