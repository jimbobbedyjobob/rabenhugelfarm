local rabenhugelFarm = ...

-- Set up the functionality of the GRAIN SILO
rabenhugelFarm:register({
    -- BUILDING Properties
    -- https://www.polymorph.games/foundation/modding/api/building
    DataType = "BUILDING_FUNCTION_WAREHOUSE",
	Id = "RAVH_BUILDING_FUNCTION__GRAIN_SILO",

	Name = "RAVH__GRAIN_SILO_FUNCTION_NAME",
	-- BUILDING_FUNCTION_WORKPLACE Properties
    --https://www.polymorph.games/foundation/modding/api/building_function_workplace
    WorkerCapacity = 2,
    UpkeepPerWorker = 
    {
        { Resource = "GOLD", Quantity = 1 } 
    },
    RelatedJob = { Job = "TRANSPORTER", Behavior = "BEHAVIOR_WORK"},
    -- BUILDING_FUNCTION_WAREHOUSE properties
    -- https://www.polymorph.games/foundation/modding/api/building_function_warehouse
    SocketCount = 4,
    SocketCapacity = 200,
    AllowedResourceTypeList = 
        {   
            "GRANARY"
        }, 
    DefaultResourceVisual = "PREFAB_RAVH_GRAINSACK_BARLEY"
})