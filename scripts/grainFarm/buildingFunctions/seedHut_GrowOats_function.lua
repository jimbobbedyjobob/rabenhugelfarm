local rabenhugelFarm = ...

-- Setup the Grow Functionality of the Seed Hut for OATS
rabenhugelFarm:register({
	DataType = "BUILDING_FUNCTION_FARM",
	Id = "RAVH_BUILDING_FUNCTION_SEEDHUT_OAT",

	Name = "RAVH_ASSIGNABLE_OAT_GROWING_FUNCTION_NAME",
-- BUILDING_FUNCTION_WORKPLACE Properties
--https://www.polymorph.games/foundation/modding/api/building_function_workplace
	WorkerCapacity = 3,
	--UpkeepPerWorker
	RelatedJob = { Job = "RAVH_FARMHAND", Behavior = "BEHAVIOR_WORK" },
    --IsPrivate
    HasResourceDepot = true,
	InputInventoryCapacity = { 
        -- how much of a resource a building can actually hold
            --{ Resource = "RAVH_SACK_GRAIN_OAT", Quantity = 1 }, -- Causes Circular Calculations in Weekly Production Calcs
			{ Resource = "WATER", Quantity = 10 },
			{ Resource = "TOOL", Quantity = 10 }
	},

	ResourceListNeeded = { 
        -- required to do work, may not apply because only certain jobs require resources in their vanilla behaviour
            --{ Resource = "RAVH_SACK_GRAIN_OAT", Quantity = 1 }, -- Causes Circular Calculations in Weekly Production Calcs
			{ Resource = "WATER", Quantity = 1 },
			{ Resource = "TOOL", Quantity = 1 }
    },
    ResourceProduced = { 
        -- used until the Farming Behaviour is opened up and it can work like a real farm
            { Resource = "RAVH_BUSHEL_OAT", Quantity = 70}
	},
-- BUILDING_FUNCTION_FARM Properties
-- https://www.polymorph.games/foundation/modding/api/building_function_farm
	SowingDurationPerc = {0, 2},
	GrowDuration = {3, 10},
	GrowWaitDuration = {11, 14},
	HarvestDuration = {15, 20},
	RestDuration = {21, 28},
	QuantityToHarvestPerStep = 30,
	HarvestStepsCount = 5,
	QuantityToPlantPerStep = 30,
	PlantingStepsCount = 5,
	ResourceValuePerGather = 1,
	CropFieldConfig = "RAVH_OAT_FIELDCONFIG",
	HarvestSetup= "RAVH_CHARACTER_GRAIN_HARVESTING_FARMER",
	SowingSetup = "RAVH_CHARACTER_GRAIN_SOWING_FARMER"
})