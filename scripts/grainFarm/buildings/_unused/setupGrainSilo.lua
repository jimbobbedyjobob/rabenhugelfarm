local rabenhugelFarm = ...

rabenhugelFarm:registerAssetProcessor("models/grainFarm/buildings/GrainSilo.fbx", {
	DataType = "BUILDING_ASSET_PROCESSOR"
})

-- register GRAIN SILO meshes - give PREFAB prefix
rabenhugelFarm:registerAssetId("models/grainFarm/buildings/GrainSilo.fbx/Prefab/GrainSilo", "PREFAB_RAVH_GRAIN_SILO")
rabenhugelFarm:registerAssetId("models/grainFarm/buildings/GrainSilo.fbx/Prefab/ConstructionSteps", "PREFAB_RAVH_GRAIN_SILO_CONSTRUCTION_STEPS")

-- register building_parts
rabenhugelFarm:register({
	DataType = "BUILDING_PART",
	Id = "RAVH_GRAIN_SILO_PART",

	ConstructorData = {
						DataType = "BUILDING_CONSTRUCTOR_DEFAULT",
						CoreObjectPrefab = "PREFAB_RAVH_GRAIN_SILO"
	},
	ConstructionVisual = "PREFAB_RAVH_GRAIN_SILO_CONSTRUCTION_STEPS",
	Cost = { 
		RessourcesNeeded = 
		{
			{ Resource = "STONE", Quantity = 10 }
		}
	},
	AssetBuildingFunction = "RAVH_BUILDING_FUNCTION__GRAIN_SILO" 
})

-- register as building
-- https://www.polymorph.games/foundation/modding/api/building
rabenhugelFarm:register({
    DataType = "BUILDING",
	Id = "RAVH_GRAIN_SILO",
	Name = "RAVH_GRAIN_SILO_NAME",
	Description = "RAVH_GRAIN_SILO_DESC",
	OrderId = 20,
	BuildingType = "GENERAL",
    BuildingPartSetList = 
    {
		{
			Name = "PREFAB_GRAIN_SILO_PARTSETLIST_NAME",
	                BuildingPartList = { "RAVH_GRAIN_SILO_PART" }
        }
    },
    --BuildingModel = "PREFAB_RAVH_GRAIN_SILO"
})

-- register construction zone
rabenhugelFarm:registerPrefabComponent("models/grainFarm/buildings/GrainSilo.fbx/Prefab/GrainSilo", 
{
	DataType = "COMP_BUILDING_PART",
	HasBuildingZone = true,
	BuildingZone = { 2, 2 }
})