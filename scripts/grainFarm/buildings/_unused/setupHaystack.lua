local rabenhugelFarm = ...

-- Register HAYSTACK

-- register Haystack meshes - give PREFAB prefix
rabenhugelFarm:registerAssetId("models/grainFarm/buildings/defaulHaystack.fbx/Prefab/SeedStorageHut", "PREFAB_RAVH_SEEDHUT")
rabenhugelFarm:registerAssetId("models/grainFarm/buildings/defaulHaystack.fbx/Prefab/ConstructionSteps", "PREFAB_RAVH_SEEDHUT_CONSTRUCTION_STEPS")