local rabenhugelFarm = ...

-- Register HORREO/ GRAIN STORE

-- register HORREO meshes as Building_Asset_Processor 
rabenhugelFarm:registerAssetProcessor("models/grainFarm/buildings/Horreo.fbx",
    {
        DataType = "BUILDING_ASSET_PROCESSOR"
    }
)

-- Register Prefab_Nodes
rabenhugelFarm:registerAssetId("models/grainFarm/buildings/Horreo.fbx/Prefab/GrainSilo_CorePart", "PREFAB_RAVH_HORREO_CORE_PART")
rabenhugelFarm:registerAssetId("models/grainFarm/buildings/Horreo.fbx/Prefab/CollectionPointPart", "PREFAB_RAVH_HORREO_HATCH_PART")
rabenhugelFarm:registerAssetId("models/grainFarm/buildings/Horreo.fbx/Prefab/GrainSilo_EndPart", "PREFAB_RAVH_HORREO_END_PART")
-- to be registered as Basic Parts
rabenhugelFarm:registerAssetId("models/grainFarm/buildings/Horreo.fbx/Prefab/GrainSilo_FillerPart_01", "PREFAB_RAVH_HORREO_TILING1_PART")
rabenhugelFarm:registerAssetId("models/grainFarm/buildings/Horreo.fbx/Prefab/GrainSilo_FillerPart_02", "PREFAB_RAVH_HORREO_TILING2_PART")
rabenhugelFarm:registerAssetId("models/grainFarm/buildings/Horreo.fbx/Prefab/GrainSilo_FillerPart_03", "PREFAB_RAVH_HORREO_TILING3_PART")
rabenhugelFarm:registerAssetId("models/grainFarm/buildings/Horreo.fbx/Prefab/GrainSilo_FillerPart_04", "PREFAB_RAVH_HORREO_TILING4_PART")
rabenhugelFarm:registerAssetId("models/grainFarm/buildings/Horreo.fbx/Prefab/GrainSilo_FillerPart_05", "PREFAB_RAVH_HORREO_TILING5_PART")
rabenhugelFarm:registerAssetId("models/grainFarm/buildings/Horreo.fbx/Prefab/GrainSilo_FillerPart_06", "PREFAB_RAVH_HORREO_TILING6_PART")

-- Register simple building part assets so they are readable by the Scaler Constructor
function registerBasicBuildingPart(_name)
    rabenhugelFarm:register({
        DataType = "BUILDING_PART",
        Id = _name .. "_BUILDING_PART",
        Mover = { DataType = "BUILDING_PART_MOVER" },
        ConstructorData = 
        {
            DataType = "BUILDING_CONSTRUCTOR_DEFAULT",
            CoreObjectPrefab = ("PREFAB_RAVH_" .. _name .. "_PART")
        }
    })
end

registerBasicBuildingPart("HORREO_CORE")
registerBasicBuildingPart("HORREO_HATCH")
registerBasicBuildingPart("HORREO_END")

registerBasicBuildingPart("HORREO_TILING1")
registerBasicBuildingPart("HORREO_TILING2")
registerBasicBuildingPart("HORREO_TILING3")
registerBasicBuildingPart("HORREO_TILING4")
registerBasicBuildingPart("HORREO_TILING5")
registerBasicBuildingPart("HORREO_TILING6")


-- Register Horreo Building Funciton



-- register Displayed Main Part with its Fillable sections
rabenhugelFarm:register({
    DataType = "BUILDING_PART",
    Id = "RAVH_HORREO_MAIN_PART",
    Name = "RAVH_HORREO_MAIN_PART_NAME",
    Description = "RAVH_HORREO_ROOT_DESC",
    Category = "CORE",
    Mover = { DataType = "BUILDING_PART_MOVER_INSTANCE" },
    --AssetBuildingFunction = "",
    -- https://www.polymorph.games/foundation/modding/api/building_constructor
    -- https://www.polymorph.games/foundation/modding/api/building_constructor_default
    -- https://www.polymorph.games/foundation/modding/api/building_constructor_scaler
    ConstructorData = {
        DataType = "BUILDING_CONSTRUCTOR_SCALER", -- Since there are different types of building constructor, 'DataType' is necessary here
        -- BUILDING_CONSTRUCTOR_DEFAULT 
        CoreObjectPrefab = "HORREO_CORE_BUILDING_PART",
        --BUILDING_CONSTRUCTOR_SCALER -- All IDs must be of Building_Part objects
		EndPart = "HORREO_END_BUILDING_PART",
        FillerList = 
        {
            "HORREO_TILING1_BUILDING_PART",
            "HORREO_TILING2_BUILDING_PART",
            "HORREO_TILING3_BUILDING_PART",
            "HORREO_TILING4_BUILDING_PART",
            "HORREO_TILING5_BUILDING_PART",
            "HORREO_TILING6_BUILDING_PART"
        },
        BasementFillerList = 
        {
            "HORREO_TILING1_BUILDING_PART"
        },
		MinimumScale = 0,
		BasementScale = 0,
        IsVertical = true,
        IsRepeatLastFiller = true
    },
    Cost = {
        UpkeepCost = 
        {
			{ Resource = "GOLD", Quantity = 1 }
		},
        RessourcesNeeded = -- List of RESOURCE_QUANTITY_PAIR
        { 
            { Resource = "STONE", Quantity = 4 },
            { Resource = "WOOD", Quantity = 12 }
		}
	},
})

--[[rabenhugelFarm:registerPrefabComponent("models/grainFarm/buildings/Horreo.fbx/Prefab/GrainSilo_RootPart",
{
    DataType = "COMP_BUILDING_PART",
	HasBuildingZone = true,
	BuildingZone = {DataType = "BUILDING_ZONE", Polygon = polygon.createRectangle({4,4})}
})
]]
-- register Grounded Components

rabenhugelFarm:registerPrefabComponent("models/grainFarm/buildings/Horreo.fbx/Prefab/CollectionPointPart", {
	DataType = "COMP_GROUNDED"
})


-- register Horreo as a Monument type building

rabenhugelFarm:register({
    DataType = "MONUMENT",
	Id = "RAVH_HORREO",
	Name = "RAVH_HORREO_MONUMENT_NAME",
	Description = "RAVH_HORREO_DESC",
	OrderId = 20,
	BuildingType = "GENERAL",
	BuildingPartSetList = {
		{
			Name = "HORREO_PARTSET_LIST_NAME",
                    BuildingPartList = 
                    { 
                        "RAVH_HORREO_MAIN_PART" -- Must be Main Part registered with the Fillables etc
                    }
        }
    },
})

rabenhugelFarm:registerPrefabComponent("models/grainFarm/buildings/Horreo.fbx/Prefab/GrainSilo_CorePart", {
	DataType = "COMP_BUILDING_PART",
	HasBuildingZone = true,
	BuildingZone = { 4, 4 }
})