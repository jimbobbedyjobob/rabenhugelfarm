local rabenhugelFarm = ...

-- Register GRAIN SILO

-- use AssetProcessor to pick up the named-nodes in the FBX
rabenhugelFarm:registerAssetProcessor("models/grainFarm/buildings/GrainSilo.fbx", {
        DataType = "BUILDING_ASSET_PROCESSOR"
})

rabenhugelFarm:log("Call building_parts scripts")
rabenhugelFarm:dofile("scripts/grainFarm/buildings/grainSilo/setupMainBuildingParts.lua")


-- register as building
    -- create a local var to contain the Building Data except for the changed Parameters
local grainSilo = 
{
    
	Id = "RAVH_GRAIN_SILO",
	Name = "RAVH_GRAIN_SILO_MONUMENT_NAME",
	Description = "RAVH_GRAIN_SILO_MONUMENT_DESC",
	OrderId = 20,
    BuildingType = BUILDING_TYPE.GENERAL,
    IsClearTrees = true
}    
	-- if statement to add changed parameters based on Version
if (foundation.getGameVersion == nil or version.cmp(foundation.getGameVersion(), "1.6") < 0) then
    rabenhugelFarm:log("< v1.6 called @ Silo")
    grainSilo.DataType = "MONUMENT"
    grainSilo.BuildingPartSetList = {
        {
            Name = "RAVH_GRAIN_SILO_MONUMENT_PARTLIST_NAME",
                    BuildingPartList =  { 
                        "GRAINSILO_MAIN_PART", -- Must be Main/Core Part registered with the Fillables etc
                        "HATCH_BUILDING_PART"
                    }
        }
    }
else
    rabenhugelFarm:log("v1.6+ called @ Silo")
    grainSilo.DataType = "BUILDING"
    grainSilo.NavMeshLockCategory = NAVMESH_LOCK_CATEGORY.NONE
    grainSilo.AssetCoreBuildingPart = "CORE_BUILDING_PART"
    grainSilo.BuildingPartSetList = {
        {
            Name = "RAVH_GRAIN_SILO_MONUMENT_PARTLIST_NAME",
                    BuildingPartList =  { 
                        "GRAINSILO_MAIN_PART", -- Must be Main/Core Part registered with the Fillables etc
                    }
        }
    }
end

-- actually call resgistry
rabenhugelFarm:register(grainSilo)

rabenhugelFarm:log("Grain Silo monument script complete")


