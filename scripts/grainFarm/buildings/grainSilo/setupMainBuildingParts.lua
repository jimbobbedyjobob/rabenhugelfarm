local rabenhugelFarm = ...

-- Register Prefabs
rabenhugelFarm:registerAssetId("models/grainFarm/buildings/GrainSilo.fbx/Prefab/00_End_Part", "RAVH_HORREO_END_PART_PREFAB")
rabenhugelFarm:registerAssetId("models/grainFarm/buildings/GrainSilo.fbx/Prefab/01_Filler_Part_A", "RAVH_HORREO_FILLER_A_PART_PREFAB")
rabenhugelFarm:registerAssetId("models/grainFarm/buildings/GrainSilo.fbx/Prefab/02_Core_Part", "RAVH_HORREO_CORE_PART_PREFAB")
rabenhugelFarm:registerAssetId("models/grainFarm/buildings/GrainSilo.fbx/Prefab/04_Basement_Part", "RAVH_HORREO_BASEMENT_PART_PREFAB")
--rabenhugelFarm:registerAssetId("models/grainFarm/buildings/GrainSilo.fbx/Prefab/06_Placer_Part", "RAVH_HORREO_PLACER_PART_PREFAB")

-- Register basic Building Part assets so they are useable by the Scaler Constructor
--https://www.polymorph.games/foundation/modding/api/building_part
rabenhugelFarm:register({
    DataType = "BUILDING_PART",
    Id = "END_BUILDING_PART",
    Mover = { DataType = "BUILDING_PART_MOVER_INSTANCE" },
    ConstructorData = 
    {
        DataType = "BUILDING_CONSTRUCTOR_DEFAULT",
        CoreObjectPrefab = ("RAVH_HORREO_END_PART_PREFAB")
    }
})
rabenhugelFarm:log("END registered as Part")

rabenhugelFarm:register({
    DataType = "BUILDING_PART",
    Id = "FILLER_A_BUILDING_PART",
    Mover = { DataType = "BUILDING_PART_MOVER_INSTANCE" },
    ConstructorData = 
    {
        DataType = "BUILDING_CONSTRUCTOR_DEFAULT",
        CoreObjectPrefab = ("RAVH_HORREO_FILLER_A_PART_PREFAB")
    }
})
rabenhugelFarm:log("FILLER_A registered as Part")

rabenhugelFarm:register({
    DataType = "BUILDING_PART",
    Id = "CORE_BUILDING_PART",
    Mover = { DataType = "BUILDING_PART_MOVER_INSTANCE" },
    ConstructorData = 
    {
        DataType = "BUILDING_CONSTRUCTOR_DEFAULT",
        CoreObjectPrefab = ("RAVH_HORREO_CORE_PART_PREFAB")
    }
})
rabenhugelFarm:log("CORE registered as Part")

rabenhugelFarm:register({
    DataType = "BUILDING_PART",
    Id = "BASEMENT_BUILDING_PART",
    Mover = { DataType = "BUILDING_PART_MOVER_INSTANCE" },
    ConstructorData = 
    {
        DataType = "BUILDING_CONSTRUCTOR_DEFAULT",
        CoreObjectPrefab = ("RAVH_HORREO_BASEMENT_PART_PREFAB")
    }
})
rabenhugelFarm:log("BASEMENT registered as Part")
--[[
rabenhugelFarm:register({
    DataType = "BUILDING_PART",
    Id = "PLACER_BUILDING_PART",
    Mover = { DataType = "BUILDING_PART_MOVER_INSTANCE" },
    ConstructorData = 
    {
        DataType = "BUILDING_CONSTRUCTOR_DEFAULT",
        CoreObjectPrefab = ("RAVH_HORREO_PLACER_PART_PREFAB")
    }
})
rabenhugelFarm:log("PLACER registered as Part")
]]
-- register Main/Core Part with its Fillable sections
--https://www.polymorph.games/foundation/modding/api/building_part
rabenhugelFarm:register({
    DataType = "BUILDING_PART",
    Id = "GRAINSILO_MAIN_PART",
    Name = "GRAINSILO_MAIN_PART_NAME",
    --Description = "SCALETSET_01_ROOT_DESC",
    Category = "CORE",
    HasMaximumInstancePerBuilding = true,
    MaximumInstancePerBuilding = 1,
    Mover = { DataType = "BUILDING_PART_MOVER_INSTANCE" },
    --AssetBuildingFunction = "",
    ConstructorData = {
         -- https://www.polymorph.games/foundation/modding/api/building_constructor
        DataType = "BUILDING_CONSTRUCTOR_SCALER", -- Since there are different types of building constructor, 'DataType' is necessary here
        -- BUILDING_CONSTRUCTOR_DEFAULT 
        -- https://www.polymorph.games/foundation/modding/api/building_constructor_default
        CoreObjectPrefab = "RAVH_HORREO_CORE_PART_PREFAB",
        --BUILDING_CONSTRUCTOR_SCALER -- All IDs must be of Building_Part objects
        -- https://www.polymorph.games/foundation/modding/api/building_constructor_scaler
		EndPart = "END_BUILDING_PART",
        FillerList = 
        {
            "FILLER_A_BUILDING_PART"
        },
        BasementFillerList = 
        {
            "BASEMENT_BUILDING_PART"
        },
        MinimumScale = 0,

        --BasementScale = 2,

        IsVertical = true,
        IsRepeatLastFiller = false
    },
    Cost = {
        UpkeepCost = 
        {
			{ Resource = "GOLD", Quantity = 1 }
		},
        RessourcesNeeded = -- List of RESOURCE_QUANTITY_PAIR
        { 
            { Resource = "STONE", Quantity = 1 },
            { Resource = "WOOD", Quantity = 1 }
		}
	},
})

-- register Grounded Component for Hatch
rabenhugelFarm:registerPrefabComponent("models/grainFarm/buildings/GrainSilo.fbx/Prefab/02_Core_Part/Hatch_LOD_01", {
	DataType = "COMP_GROUNDED"
})

-- register Building Zone Component
rabenhugelFarm:registerPrefabComponent("models/grainFarm/buildings/GrainSilo.fbx/Prefab/02_Core_Part", {
	DataType = "COMP_BUILDING_PART",
	HasBuildingZone = true,
	BuildingZone = { 4, 4 }
})

rabenhugelFarm:log("Grain Silo Main Building Parts script complete")

