local rabenhugelFarm = ...

-- Register SEED HUT

-- use AssetProcessor to pick up the named-nodes in the FBX
rabenhugelFarm:registerAssetProcessor("models/grainFarm/buildings/seedStorageHut.fbx", {
	DataType = "BUILDING_ASSET_PROCESSOR"
})

-- register seed hut meshes - give PREFAB prefix
rabenhugelFarm:registerAssetId("models/grainFarm/buildings/seedStorageHut.fbx/Prefab/SeedStorageHutPart", "PREFAB_RAVH_SEEDHUT")
rabenhugelFarm:registerAssetId("models/grainFarm/buildings/seedStorageHut.fbx/Prefab/ConstructionStepsPart", "PREFAB_RAVH_SEEDHUT_CONSTRUCTION_STEPS")

-- register seed hut building_parts
rabenhugelFarm:register({
	DataType = "BUILDING_PART",
	Id = "RAVH_SEEDHUT_PART",
	Mover = { DataType = "BUILDING_PART_MOVER_INSTANCE" },
	ConstructorData = {
						DataType = "BUILDING_CONSTRUCTOR_DEFAULT",
						CoreObjectPrefab = "PREFAB_RAVH_SEEDHUT"
	},
	ConstructionVisual = "PREFAB_RAVH_SEEDHUT_CONSTRUCTION_STEPS",
	Cost = { 
		RessourcesNeeded = 
		{
			{ Resource = "STONE", Quantity = 2 },
			{ Resource = "WOOD", Quantity = 3 }
		}
	},
	AssetBuildingFunction = "BUILDING_FUNCTION_ASSIGNABLE_RAVH_SEEDHUT" 
})

---------------------------------------------------------------------------------------------------------

-- register seed hut as building
	-- create a local var to contain the Building Data except for the changed Parameters
local seedHut = 
{
	DataType = "BUILDING",
	Id = "RAVH_SEEDHUT",
	Name = "RAVH_SEEDHUT_NAME",
	Description = "RAVH_SEEDHUT_DESC",
	OrderId = 20,
	BuildingType = BUILDING_TYPE.GENERAL,
}
	-- if statement to add changed parameters based on Version
if (foundation.getGameVersion == nil or version.cmp(foundation.getGameVersion(), "1.6") < 0) then
	rabenhugelFarm:log("< v1.6 called @ Seed")
	seedHut.BuildingPartSetList = {
		{
			Name = "PREFAB_SEEDHUT_PARTSETLIST_NAME",
					BuildingPartList = { "RAVH_SEEDHUT_PART" }
		}
	}
else
	rabenhugelFarm:log("v1.6+ called @ Seed")
	seedHut.AssetCoreBuildingPart = "RAVH_SEEDHUT_PART"
end
	-- actually call resgistry
rabenhugelFarm:register(seedHut)

---------------------------------------------------------------------------------------------------------

-- Register Assignable Funciton List
rabenhugelFarm:register({
	DataType = "BUILDING_FUNCTION_LIST",
	Id = "BUILDING_FUNCTION_RAVH_SEEDHUT",

	AssetFunctionList = { 	"RAVH_BUILDING_FUNCTION_SEEDHUT_BARLEY",
							"RAVH_BUILDING_FUNCTION_SEEDHUT_OAT",
							"RAVH_BUILDING_FUNCTION_SEEDHUT_RYE",
							"RAVH_BUILDING_FUNCTION_SEEDHUT_WHEAT"}
})

rabenhugelFarm:register({
	DataType = "BUILDING_FUNCTION_ASSIGNABLE",
	Id = "BUILDING_FUNCTION_ASSIGNABLE_RAVH_SEEDHUT",

	AuthorizedFunctionList = "BUILDING_FUNCTION_RAVH_SEEDHUT"
})

rabenhugelFarm:registerPrefabComponent("models/grainFarm/buildings/seedStorageHut.fbx/Prefab/SeedStorageHutPart", 
{
	DataType = "COMP_FARM",
})

-- register seed hut construction zone
--https://www.polymorph.games/foundation/modding/api/comp_building_part?s[]=buildingzonedata
rabenhugelFarm:registerPrefabComponent("models/grainFarm/buildings/seedStorageHut.fbx/Prefab/SeedStorageHutPart", 
{
	DataType = "COMP_BUILDING_PART",
	HasBuildingZone = true,
	BuildingZoneData = 	{
		Polygon =
		{
			{ -1.3, 1.2 }, 
			{ 1.0, 1.2 }, 
			{ 1.3, 0.7 },
			{ 3.2, 0.6 }, -- Front Right
			{ 3.2, -0.6 }, -- Front Left
			{ 1.3, -0.7 }, 
			{ 1.0, -1.2 }, 
			{ -1.3, -1.2 }
		}
	}
})

rabenhugelFarm:registerPrefabComponent("models/grainFarm/buildings/seedStorageHut.fbx/Prefab/SeedStorageHutPart", 
{
	DataType = "COMP_DIRT_CIRCLE",	
    HardRadius = 1.0,
    SoftRadius = 1.5,
    Strength = 0.5,
    IsPermanent = true
})

-- Register Platform for navmesh
rabenhugelFarm:configurePrefabFlagList("models/grainFarm/buildings/seedStorageHut.fbx/Prefab/SeedStorageHutPart", {"PLATFORM"})