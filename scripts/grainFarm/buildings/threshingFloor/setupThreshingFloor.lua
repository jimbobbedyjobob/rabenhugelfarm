local rabenhugelFarm = ...

-- Register THRESHING FLOOR

-- use AssetProcessor to pick up the named-nodes in the FBX
rabenhugelFarm:registerAssetProcessor("models/grainFarm/buildings/threshingFloor.fbx", {
	DataType = "BUILDING_ASSET_PROCESSOR"
})

-- register THRESHING FLOOR meshes - give PREFAB prefix
rabenhugelFarm:registerAssetId("models/grainFarm/buildings/threshingFloor.fbx/Prefab/ThreshingFloorPart", "PREFAB_RAVH_THRESHINGFLOOR")
rabenhugelFarm:registerAssetId("models/grainFarm/buildings/threshingFloor.fbx/Prefab/ConstructionSteps", "PREFAB_RAVH_THRESHINGFLOOR_CONSTRUCTION_STEPS")

-- register building_parts
rabenhugelFarm:register({
	DataType = "BUILDING_PART",
	Id = "RAVH_THRESHINGFLOOR_PART",
	Mover = { DataType = "BUILDING_PART_MOVER_INSTANCE" },
	ConstructorData = {
						DataType = "BUILDING_CONSTRUCTOR_DEFAULT",
						CoreObjectPrefab = "PREFAB_RAVH_THRESHINGFLOOR"
	},
	ConstructionVisual = "PREFAB_RAVH_THRESHINGFLOOR_CONSTRUCTION_STEPS",
	Cost = { 
		RessourcesNeeded = 
		{
			{ Resource = "STONE", Quantity = 10 }
		}
	},
	AssetBuildingFunction = "BUILDING_FUNCTION_ASSIGNABLE_RAVH_THRESHINGFLOOR" 
})

---------------------------------------------------------------------------------------------------------

-- register as building
	-- create a local var to contain the Building Data except for the changed Parameters
local threshingFloor = 
{
    DataType = "BUILDING",
	Id = "RAVH_THRESHINGFLOOR",
	Name = "RAVH_THRESHINGFLOOR_NAME",
	Description = "RAVH_THRESHINGFLOOR_DESC",
	OrderId = 20,
	BuildingType = BUILDING_TYPE.GENERAL,
}
	-- if statement to add changed parameters based on Version
if (foundation.getGameVersion == nil or version.cmp(foundation.getGameVersion(), "1.6") < 0) then
	rabenhugelFarm:log("< v1.6 called @ Thresher")
	threshingFloor.BuildingPartSetList = {
		{
			Name = "PREFAB_THRESHINGFLOOR_PARTSETLIST_NAME",
					BuildingPartList = { "RAVH_THRESHINGFLOOR_PART" }
		}
	}
else
	rabenhugelFarm:log("v1.6+ called @ Thresher")
	threshingFloor.AssetCoreBuildingPart = "RAVH_THRESHINGFLOOR_PART"
end
	-- actually call resgistry
rabenhugelFarm:register(threshingFloor)

---------------------------------------------------------------------------------------------------------

-- Register Assignable Function List
rabenhugelFarm:register({
	DataType = "BUILDING_FUNCTION_LIST",
	Id = "BUILDING_FUNCTION_RAVH_THRESHINGFLOOR",

	AssetFunctionList = { 	"RAVH_BUILDING_FUNCTION_THRESHINGFLOOR_BARLEY",
							"RAVH_BUILDING_FUNCTION_THRESHINGFLOOR_OATS",
							"RAVH_BUILDING_FUNCTION_THRESHINGFLOOR_RYE",
							"RAVH_BUILDING_FUNCTION_THRESHINGFLOOR_WHEAT"}
})

rabenhugelFarm:register({
	DataType = "BUILDING_FUNCTION_ASSIGNABLE",
	Id = "BUILDING_FUNCTION_ASSIGNABLE_RAVH_THRESHINGFLOOR",

	AuthorizedFunctionList = "BUILDING_FUNCTION_RAVH_THRESHINGFLOOR"
})

-- register construction zone
	-- register seed hut construction zone
	--https://www.polymorph.games/foundation/modding/api/comp_building_part?s[]=buildingzonedata
rabenhugelFarm:registerPrefabComponent("models/grainFarm/buildings/threshingFloor.fbx/Prefab/ThreshingFloorPart", 
{
	DataType = "COMP_BUILDING_PART",
	HasBuildingZone = true,
	BuildingZoneData = 	{
		Polygon =
		{
			{ -2.74, 0 }, -- Centre Back
			{ -2.53143, 1.0486 }, -- Circling Clockwise when looking down
			{ -1.93747, 1.93747 }, 
			{ -1.0486, 2.53143 }, 
			{ 0, 2.74 }, -- 3 o'clock Centre Front Needs to be here
			{ 1.04855, 2.53143 },
			{ 1.93747, 1.93747 },
			{ 4.30925, 1.0486 },
			{ 4.51469, 0 }, -- Centre Front
			{ 4.30925, -1.0486 },
			{ 1.93747, -1.93747 },
			{ 0.4855, -2.53143 },
			{ 0, -2.74 }, -- 9 o'clock
			{ -1.0486, -2.53143 },
			{ -1.93747, -1.93747 },
			{ -2.53143, -1.0486 }
		}
	}
})

-- https://www.polymorph.games/foundation/modding/api/comp_dirt_circle
rabenhugelFarm:registerPrefabComponent("models/grainFarm/buildings/threshingFloor.fbx/Prefab/ThreshingFloorPart", 
{
	DataType = "COMP_DIRT_CIRCLE",	
    HardRadius = 1.8,
    SoftRadius = 2.0,
    Strength = 1.0,
    IsPermanent = true
})

-- Register Platform for navmesh
rabenhugelFarm:configurePrefabFlagList("models/grainFarm/buildings/threshingFloor.fbx/Prefab/ThreshingFloorPart", {"PLATFORM"})