local rabenhugelFarm = ...

--- Register Grain Farmer Job. . . May need grain-specific farm jobs if I can't work out how to choose one based on attached zone

rabenhugelFarm:register({
    DataType = "JOB",
    Id = "RAVH_TENNANT_FARMER",

    JobName = "RAVH_TENNANT_FARMER_NAME",
    JobDescription = "RAVH_TENNANT_FARMER_DESC",

    UseWorkplaceBehavior = true,
    DefaultBehavior = "WORK_BEHAVIOR",
    ProductionDelay = 150.0,
    --RelatedZone = "BARELY_FIELD_ZONE", -- Does This Cause Farming Behaviours?

    NeededMasteredJobList = {
        "RAVH_FARMHAND"
    },
    ExperienceToMaster = 32,
    AssetJobProgression = "BASIC_JOB_PROGRESSION_LIST",

    CharacterSetup = genericFarmhand,
    Hidden = false,
    IsDefinitive = false,
    IsLockedByDefault = false,
    --AssetNoZoneNotification = -- https://www.polymorph.games/foundation/modding/api/notification
    --AssetNoTreeChoiceNotification = -- https://www.polymorph.games/foundation/modding/api/notification
    --AssetNoResourceInZoneNotification = -- https://www.polymorph.games/foundation/modding/api/notification
})