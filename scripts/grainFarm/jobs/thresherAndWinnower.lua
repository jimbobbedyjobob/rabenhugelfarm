local rabenhugelFarm = ...

--- Register Thresher and Winnower Job. . . May need grain-specific farm jobs if I can't work out how to choose one based on attached zone

rabenhugelFarm:register({
    DataType = "JOB",
    Id = "RAVH_THRESHERWINNOWER",

    JobName = "RAVH_THRESHERWINNOWER_NAME",
    JobDescription = "RAVH_THRESHERWINNOWER_DESC",

    UseWorkplaceBehavior = true,
    DefaultBehavior = "WORK_BEHAVIOR",
    ProductionDelay = 30.0,
    --RelatedZone = "",

    -- NeededMasteredJobList = list<JOB>
    ExperienceToMaster = 32,
    AssetJobProgression = "BASIC_JOB_PROGRESSION_LIST",

    CharacterSetup = grainThresher,
    Hidden = false,
    IsDefinitive = false,
    IsLockedByDefault = false,
    --AssetNoZoneNotification = -- https://www.polymorph.games/foundation/modding/api/notification
    --AssetNoTreeChoiceNotification = -- https://www.polymorph.games/foundation/modding/api/notification
    --AssetNoResourceInZoneNotification = -- https://www.polymorph.games/foundation/modding/api/notification
})

