local rabenhugelFarm = ...

-- AGENT_WORKSTATION_SETUPs required by Building_FunctionFarm --https://www.polymorph.games/foundation/modding/api/building_function_farm
-- https://www.polymorph.games/foundation/modding/api/agent_workstation_setup
rabenhugelFarm:register({
	DataType = "AGENT_WORKSTATION_SETUP",
    Id = "RAVH_CHARACTER_GRAIN_HARVESTING_FARMER",

    CharacterSetup = farmhandHarvesting
})

rabenhugelFarm:register({
	DataType = "AGENT_WORKSTATION_SETUP",
    Id = "RAVH_CHARACTER_GRAIN_SOWING_FARMER",

    CharacterSetup = farmhandSowing
})

rabenhugelFarm:log("Grain Farm Agents Registered")
