local rabenhugelFarm = ...

-- CHARACTER_SETUP - Grain Harvesting Farmer
--https://www.polymorph.games/foundation/modding/api/character_setup

return{
    LeftHandObject = nil,
    RightHandObject = grainFarmerScythe,

    --BagModel = nil,
    --ForceNoBag = true,

    MaleHatModel = nil,
    ForceNoMaleHat = false,

    FemaleHatModel = nil,
    ForceNoFemaleHat = false,

    MaleBeltModel = nil,
    ForceNoMaleBelt = false,

    FemaleBeltModel = nil,
    ForceNoFemaleBelt = false,

    MaleClothingModel = nil, -- Prefab List,
    FemaleClothingModel = nil, -- Prefab List,

    MaleHairList = nil, --https://www.polymorph.games/foundation/modding/api/hair_list
    FemaleHairList = nil, --https://www.polymorph.games/foundation/modding/api/hair_list

    BeardList = nil, --https://www.polymorph.games/foundation/modding/api/hair_list
    MustacheList = nil, --https://www.polymorph.games/foundation/modding/api/hair_list

    MaleHairMaterialList = nil, -- Material List,
    FemaleHairMaterialList = nil, -- Material List,

    WorkAnimation = "SCYTHE", --https://www.polymorph.games/foundation/modding/api/agent_animation
    WalkAnimation = "SCYTHE_WALK", --https://www.polymorph.games/foundation/modding/api/agent_animation
    IdleAnimation = "IDLE"  --https://www.polymorph.games/foundation/modding/api/agent_animation
}