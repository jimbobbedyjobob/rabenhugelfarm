local rabenhugelFarm = ...

--- Register Grain Farmer Job. . . May need grain-specific farm jobs if I can't work out how to choose one based on attached zone

rabenhugelFarm:register({
    DataType = "JOB",
    Id = "RAVH_FARMHAND",

    JobName = "RAVH_FARMHAND_NAME",
    JobDescription = "RAVH_FARMHAND_DESC",

    UseWorkplaceBehavior = true,
    DefaultBehavior = "WORK_BEHAVIOR",
    ProductionDelay = 150.0,
    --RelatedZone = "BARELY_FIELD_ZONE", -- Does This Cause Farming Behaviours?

    -- NeededMasteredJobList = list<JOB>
    ExperienceToMaster = 32,
    AssetJobProgression = "BASIC_JOB_PROGRESSION_LIST",

    CharacterSetup = genericFarmhand,
    Hidden = false,
    IsDefinitive = false,
    IsLockedByDefault = false,
    --AssetNoZoneNotification = -- https://www.polymorph.games/foundation/modding/api/notification
    --AssetNoTreeChoiceNotification = -- https://www.polymorph.games/foundation/modding/api/notification
    --AssetNoResourceInZoneNotification = -- https://www.polymorph.games/foundation/modding/api/notification
})