local rabenhugelFarm = ...

-- Register all Tool HAND_OBJECT tables
-- https://www.polymorph.games/foundation/modding/api/hand_object

return{
    DataType = "HAND_OBJECT",
    Id = "RAVH_HANDOBJECT_SICKLE",

    Model = "PREFAB_TOOL_FARMER_SICKLE",
    -- WorkAttachName = ""
    -- WalkAttachName = ""
    KeepObjectOnWalkCycle = true 
}