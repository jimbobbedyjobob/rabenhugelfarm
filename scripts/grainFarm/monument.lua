local rabenhugelFarm = ...

-- Register The Grain Farm as a Monument of type General
rabenhugelFarm:register({
	DataType = "MONUMENT",
	Id = "RAVH_HORREO",
	Name = "RAVH_GRAIN_FARM_MONUMENT_NAME",
	Description = "RAVH_GRAIN_FARM_MONUMENT_DESC",
	BuildingType = "GENERAL",
    BuildingPartSetList = 
    {
		{	
			Name = "HORREO_PARTSET_LIST_NAME",
            	BuildingPartList = 
                {
                    "RAVH_HORREO_PARTS_LIST" -- red by default (for ascending compatibility reason)                    
                }
        }
    },
	RequiredPartList = 
	{
		-- Necessarily a list of 'MONUMENT_REQUIRED_PART_PAIR', no need to specify the DataType of each element of the list
		{ Category = "CORE", Quantity = 1 }
	},
        IsEditable = true
})
