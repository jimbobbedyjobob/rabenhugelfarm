local rabenhugelFarm = ...

-- Register all Assets required for OAT FIELD CONFIG

-- Register TEXTURES
-- https://www.polymorph.games/foundation/modding/api/texture
-- growing
rabenhugelFarm:registerAssetId("textures/grainFarm/oat_growingTex_01.png", "RAVH_GROWING_OAT_TEX01") -- REPLACE!!
rabenhugelFarm:registerAssetId("textures/grainFarm/oat_growingTex_02.png", "RAVH_GROWING_OAT_TEX02") -- REPLACE!!
rabenhugelFarm:registerAssetId("textures/grainFarm/oat_growingTex_03.png", "RAVH_GROWING_OAT_TEX03") -- REPLACE!!
rabenhugelFarm:registerAssetId("textures/grainFarm/oat_growingTex_04.png", "RAVH_GROWING_OAT_TEX04") -- REPLACE!!
-- harvesting
rabenhugelFarm:registerAssetId("textures/grainFarm/oat_harvestingTex_01.png", "RAVH_HARVESTING_OAT_TEX01") -- REPLACE!!
rabenhugelFarm:registerAssetId("textures/grainFarm/oat_harvestingTex_02.png", "RAVH_HARVESTING_OAT_TEX02") -- REPLACE!!
rabenhugelFarm:registerAssetId("textures/grainFarm/oat_harvestingTex_03.png", "RAVH_HARVESTING_OAT_TEX03") -- REPLACE!!
rabenhugelFarm:registerAssetId("textures/grainFarm/oat_harvestingTex_04.png", "RAVH_HARVESTING_OAT_TEX04") -- REPLACE!!

-- Register Texture Lists
OatGrowingTexs_RAVH = { "RAVH_GROWING_OAT_TEX01",
                        "RAVH_GROWING_OAT_TEX02", 
                        "RAVH_GROWING_OAT_TEX02", 
                        "RAVH_GROWING_OAT_TEX04"
}
OatHarvestingTexs_RAVH = {  "RAVH_HARVESTING_OAT_TEX01", 
                            "RAVH_HARVESTING_OAT_TEX02", 
                            "RAVH_HARVESTING_OAT_TEX03", 
                            "RAVH_HARVESTING_OAT_TEX04"
}

-- Register MATERIALS
-- https://www.polymorph.games/foundation/modding/api/material
rabenhugelFarm:register({
    DataType = "MATERIAL",
    Id = "OAT_FIELD_SOURCEMATERIAL",

    HasTransparency = true,
    IsLighted = true,
    BackFaceVisible = true,
    AlbedoTexture = "RAVH_HARVESTING_OAT_TEX04" -- REPLACE with Source when I know what the hell it does
})
rabenhugelFarm:register({
    DataType = "MATERIAL",
    Id = "OAT_FIELD_GROWINGMATERIAL",

    HasTransparency = true,
    IsLighted = true,
    BackFaceVisible = true,
    AlbedoTexture = "RAVH_GROWING_OAT_TEX01"
})
rabenhugelFarm:register({
    DataType = "MATERIAL",
    Id = "OAT_FIELD_HARVESTEDMATERIAL",

    HasTransparency = true,
    IsLighted = true,
    BackFaceVisible = true,
    AlbedoTexture = "RAVH_HARVESTING_OAT_TEX01"
})

-- Register FIELD TYPE
-- https://www.polymorph.games/foundation/modding/api/farm_field_config
rabenhugelFarm:register({
	DataType = "FARM_FIELD_CONFIG",
    Id = "RAVH_OAT_FIELDCONFIG",

    ZoneType = "OAT_FIELD_ZONE",
    PlantPrefab = "PREFAB_RAVH_OAT_PLANT",
        
    HasPlantingCycle = true,
    IsScaling = true,
    RowDistance = 3.0,
    ItemDistance = 2.0,
    RandomOrientation = {1.0, 1.0},
    RamdomPositionOffset = {1.0, 1.0},

    SourceMaterial = "OAT_FIELD_SOURCEMATERIAL",
    GrowingMaterial = "OAT_FIELD_GROWINGMATERIAL",
    HarvestedMaterial = "OAT_FIELD_HARVESTEDMATERIAL",

    GrowingTextureList = OatGrowingTexs_RAVH,
    HarvestingTextureList = OatHarvestingTexs_RAVH
})