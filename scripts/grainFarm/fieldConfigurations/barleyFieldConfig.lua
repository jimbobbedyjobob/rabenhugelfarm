local rabenhugelFarm = ...

-- Register all Assets required for BARLEY FIELD CONFIG

-- Register TEXTURES
-- https://www.polymorph.games/foundation/modding/api/texture
-- growing
rabenhugelFarm:registerAssetId("textures/grainFarm/barley_growingTex_01.png", "RAVH_GROWING_BARLEY_TEX01")
rabenhugelFarm:registerAssetId("textures/grainFarm/barley_growingTex_02.png", "RAVH_GROWING_BARLEY_TEX02")
rabenhugelFarm:registerAssetId("textures/grainFarm/barley_growingTex_03.png", "RAVH_GROWING_BARLEY_TEX03")
rabenhugelFarm:registerAssetId("textures/grainFarm/barley_growingTex_04.png", "RAVH_GROWING_BARLEY_TEX04")
-- harvesting
rabenhugelFarm:registerAssetId("textures/grainFarm/barley_harvestingTex_01.png", "RAVH_HARVESTING_BARLEY_TEX01")
rabenhugelFarm:registerAssetId("textures/grainFarm/barley_harvestingTex_02.png", "RAVH_HARVESTING_BARLEY_TEX02")
rabenhugelFarm:registerAssetId("textures/grainFarm/barley_harvestingTex_03.png", "RAVH_HARVESTING_BARLEY_TEX03")
rabenhugelFarm:registerAssetId("textures/grainFarm/barley_harvestingTex_04.png", "RAVH_HARVESTING_BARLEY_TEX04")

-- Register Texture Lists
BarleyGrowingTexs_RAVH = {  "RAVH_GROWING_BARLEY_TEX01", 
                            "RAVH_GROWING_BARLEY_TEX02", 
                            "RAVH_GROWING_BARLEY_TEX02", 
                            "RAVH_GROWING_BARLEY_TEX04"
}
BarleyHarvestingTexs_RAVH = {   "RAVH_HARVESTING_BARLEY_TEX01", 
                                "RAVH_HARVESTING_BARLEY_TEX02", 
                                "RAVH_HARVESTING_BARLEY_TEX03", 
                                "RAVH_HARVESTING_BARLEY_TEX04"
}

-- Register MATERIALS
-- https://www.polymorph.games/foundation/modding/api/material
rabenhugelFarm:register({
    DataType = "MATERIAL",
    Id = "BARELEY_FIELD_SOURCEMATERIAL",

    HasTransparency = true,
    IsLighted = true,
    BackFaceVisible = true,
    AlbedoTexture = "RAVH_HARVESTING_BARLEY_TEX04" -- REPLACE with Source when I know what the hell it does
})
rabenhugelFarm:register({
    DataType = "MATERIAL",
    Id = "BARELEY_FIELD_GROWINGMATERIAL",

    HasTransparency = true,
    IsLighted = true,
    BackFaceVisible = true,
    AlbedoTexture = "RAVH_GROWING_BARLEY_TEX01"
})
rabenhugelFarm:register({
    DataType = "MATERIAL",
    Id = "BARELEY_FIELD_HARVESTEDMATERIAL",

    HasTransparency = true,
    IsLighted = true,
    BackFaceVisible = true,
    AlbedoTexture = "RAVH_HARVESTING_BARLEY_TEX01"
})

-- Register FIELD CONFIG
-- https://www.polymorph.games/foundation/modding/api/farm_field_config
rabenhugelFarm:register({
	DataType = "FARM_FIELD_CONFIG",
    Id = "RAVH_BARLEY_FIELDCONFIG",

    ZoneType = "BARELY_FIELD_ZONE",
    PlantPrefab = "PREFAB_RAVH_BARLEY_PLANT",
    
    HasPlantingCycle = true,
    IsScaling = true,
    RowDistance = 3.0,
    ItemDistance = 2.0,
    RandomOrientation = {1.0, 1.0},
    RamdomPositionOffset = {1.0, 1.0},

    SourceMaterial = "BARELEY_FIELD_SOURCEMATERIAL",
    GrowingMaterial = "BARELEY_FIELD_GROWINGMATERIAL",
    HarvestedMaterial = "BARELEY_FIELD_HARVESTEDMATERIAL",

    GrowingTextureList = BarleyGrowingTexs_RAVH,
    HarvestingTextureList = BarleyHarvestingTexs_RAVH
})