local rabenhugelFarm = ...

-- Register all Assets required for RYE FIELD CONFIG

-- Register TEXTURES
-- https://www.polymorph.games/foundation/modding/api/texture
-- growing
rabenhugelFarm:registerAssetId("textures/grainFarm/rye_growingTex_01.png", "RAVH_GROWING_RYE_TEX01") -- REPLACE!!
rabenhugelFarm:registerAssetId("textures/grainFarm/rye_growingTex_02.png", "RAVH_GROWING_RYE_TEX02") -- REPLACE!!
rabenhugelFarm:registerAssetId("textures/grainFarm/rye_growingTex_03.png", "RAVH_GROWING_RYE_TEX03") -- REPLACE!!
rabenhugelFarm:registerAssetId("textures/grainFarm/rye_growingTex_04.png", "RAVH_GROWING_RYE_TEX04") -- REPLACE!!
-- harvesting
rabenhugelFarm:registerAssetId("textures/grainFarm/rye_harvestingTex_01.png", "RAVH_HARVESTING_RYE_TEX01") -- REPLACE!!
rabenhugelFarm:registerAssetId("textures/grainFarm/rye_harvestingTex_02.png", "RAVH_HARVESTING_RYE_TEX02") -- REPLACE!!
rabenhugelFarm:registerAssetId("textures/grainFarm/rye_harvestingTex_03.png", "RAVH_HARVESTING_RYE_TEX03") -- REPLACE!!
rabenhugelFarm:registerAssetId("textures/grainFarm/rye_harvestingTex_04.png", "RAVH_HARVESTING_RYE_TEX04") -- REPLACE!!

-- Register Texture Lists
RyeGrowingTexs_RAVH = { "RAVH_GROWING_RYE_TEX01",
                        "RAVH_GROWING_RYE_TEX02", 
                        "RAVH_GROWING_RYE_TEX02", 
                        "RAVH_GROWING_RYE_TEX04"
}
RyeHarvestingTexs_RAVH = {  "RAVH_HARVESTING_RYE_TEX01", 
                            "RAVH_HARVESTING_RYE_TEX02", 
                            "RAVH_HARVESTING_RYE_TEX03", 
                            "RAVH_HARVESTING_RYE_TEX04"
}

-- Register MATERIALS
-- https://www.polymorph.games/foundation/modding/api/material
rabenhugelFarm:register({
    DataType = "MATERIAL",
    Id = "RYE_FIELD_SOURCEMATERIAL",

    HasTransparency = true,
    IsLighted = true,
    BackFaceVisible = true,
    AlbedoTexture = "RAVH_HARVESTING_RYE_TEX04" -- REPLACE with Source when I know what the hell it does
})
rabenhugelFarm:register({
    DataType = "MATERIAL",
    Id = "RYE_FIELD_GROWINGMATERIAL",

    HasTransparency = true,
    IsLighted = true,
    BackFaceVisible = true,
    AlbedoTexture = "RAVH_GROWING_RYE_TEX01"
})
rabenhugelFarm:register({
    DataType = "MATERIAL",
    Id = "RYE_FIELD_HARVESTEDMATERIAL",

    HasTransparency = true,
    IsLighted = true,
    BackFaceVisible = true,
    AlbedoTexture = "RAVH_HARVESTING_RYE_TEX01"
})

-- Register FIELD TYPE
-- https://www.polymorph.games/foundation/modding/api/farm_field_config
rabenhugelFarm:register({
	DataType = "FARM_FIELD_CONFIG",
    Id = "RAVH_RYE_FIELDCONFIG",

    ZoneType = "RYE_FIELD_ZONE",
    PlantPrefab = "PREFAB_RAVH_RYE_PLANT",
    
    HasPlantingCycle = true,
    IsScaling = true,
    RowDistance = 3.0,
    ItemDistance = 2.0,
    RandomOrientation = {1.0, 1.0},
    RamdomPositionOffset = {1.0, 1.0},

    SourceMaterial = "RYE_FIELD_SOURCEMATERIAL",
    GrowingMaterial = "RYE_FIELD_GROWINGMATERIAL",
    HarvestedMaterial = "RYE_FIELD_HARVESTEDMATERIAL",

    GrowingTextureList = RyeGrowingTexs_RAVH,
    HarvestingTextureList = RyeHarvestingTexs_RAVH
})