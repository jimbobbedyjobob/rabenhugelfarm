local rabenhugelFarm = ...

-- Register all meshes, icons and resources for Barrels of ALE

-- Register MODELS
rabenhugelFarm:registerAssetId("models/higherLevelResources/BarrelRavh.fbx/Prefab/Ravh_Barrel","PREFAB_RAVH_ALE")

-- Register ICONS
rabenhugelFarm:registerAssetId("icons/higherLevelResources/ale_ravh_icon.png", "RAVH_ALE_ICON")

-- Register RESOURCES
-- SACKS of GRAIN
rabenhugelFarm:register({
	DataType = "RESOURCE",
	Id = "RAVH_ALE",
	ResourceName = "RAVH_ALE_NAME",
	Icon = "RAVH_ALE_ICON",
    ResourceTypeList = { "TAVERN", "GRANARY" }, -- Hopefully this makes it work like Wine and Vanilla Beer
	IsTradable = true,
	SellingPrice = 30.0,
	BuyingPrice = 40.0,
	VillagerBuyingPrice =     
	{
        Resource = "GOLD",
        Quantity = 35
    },
	ResourceLayoutType = "OPEN_BAGS",
	Prefab = "PREFAB_RAVH_ALE",
	DisplayInToolbar = true
})