local rabenhugelFarm = ...

-- Register all meshes, icons and resources for Grain SACKS

-- Register MODELS
rabenhugelFarm:registerAssetId("models/higherLevelResources/crateBarleyBread.fbx/Prefab/BarleyBreadCrate","PREFAB_RAVH_BREAD_BARLEY")
rabenhugelFarm:registerAssetId("models/higherLevelResources/crateRyeBread.fbx/Prefab/RyeBreadCrate","PREFAB_RAVH_BREAD_RYE")
rabenhugelFarm:registerAssetId("models/higherLevelResources/crateWheatBread.fbx/Prefab/WheatBreadCrate","PREFAB_RAVH_BREAD_WHEAT")

-- Register ICONS
rabenhugelFarm:registerAssetId("icons/higherLevelResources/bread_barley_icon.png", "RAVH_BREAD_BARLEY_ICON")
rabenhugelFarm:registerAssetId("icons/higherLevelResources/bread_rye_icon.png", "RAVH_BREAD_RYE_ICON")
rabenhugelFarm:registerAssetId("icons/higherLevelResources/bread_wheat_icon.png", "RAVH_BREAD_WHEAT_ICON")

-- Register RESOURCES
-- SACKS of GRAIN
rabenhugelFarm:register({
	DataType = "RESOURCE",
	Id = "RAVH_BREAD_BARLEY",
	ResourceName = "RAVH_BREAD_BARLEY_NAME",
	Icon = "RAVH_BREAD_BARLEY_ICON",
	ResourceTypeList = { "FOOD" }, -- Can be stored in Granary and can be sold at Market Stalls and can fulfill Food Needs
	IsTradable = true,
	SellingPrice = 6.5,
	BuyingPrice = 8.5,
	VillagerBuyingPrice =     
	{
        Resource = "GOLD",
        Quantity = 8
    },
	ResourceLayoutType = "CRATES",
	Prefab = "PREFAB_RAVH_BREAD_BARLEY",
	DisplayInToolbar = true
})

rabenhugelFarm:register({
	DataType = "RESOURCE",
	Id = "RAVH_BREAD_RYE",
	ResourceName = "RAVH_BREAD_RYE_NAME",
	Icon = "RAVH_BREAD_RYE_ICON",
	ResourceTypeList = { "FOOD" },
	IsTradable = true,
	SellingPrice = 8.5,
	BuyingPrice = 10.5,
	VillagerBuyingPrice =     
	{
        Resource = "GOLD",
        Quantity = 10
    },
	ResourceLayoutType = "CRATES",
	Prefab = "PREFAB_RAVH_BREAD_RYE",
	DisplayInToolbar = true
})

rabenhugelFarm:register({
	DataType = "RESOURCE",
	Id = "RAVH_BREAD_WHEAT",
	ResourceName = "RAVH_BREAD_WHEAT_NAME",
	Icon = "RAVH_BREAD_WHEAT_ICON",
	ResourceTypeList = { "LUXURY_FOOD" },
	IsTradable = true,
	SellingPrice = 11.5,
	BuyingPrice = 12.5,
	VillagerBuyingPrice =     
	{
        Resource = "GOLD",
        Quantity = 12
    },
	ResourceLayoutType = "CRATES",
	Prefab = "PREFAB_RAVH_BREAD_WHEAT",
	DisplayInToolbar = true
})