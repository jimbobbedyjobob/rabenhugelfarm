local rabenhugelFarm = ...

-- Add any new Resource Types to the core list

rabenhugelFarm:override({
    Id = "RESOURCE_TYPE",
    {
        Action = "APPEND",
        { "GRAIN" }
    }
})

rabenhugelFarm:log("New Resource Types Added")