local rabenhugelFarm = ...

rabenhugelFarm:override({
    Id = "NEWCOMER",
    CompatibleJobList = {
        Action = "APPEND",
        "RAVH_FARMHAND",
        "RAVH_THRESHERWINNOWER"
    }
})

rabenhugelFarm:override({
    Id = "SERF",
    CompatibleJobList = {
        Action = "APPEND",
        "RAVH_FARMHAND",
        "RAVH_THRESHERWINNOWER"
    }
})

rabenhugelFarm:override({
    Id = "COMMONER",
    CompatibleJobList = {
        Action = "APPEND",
        "RAVH_FARMHAND",
        "RAVH_THRESHERWINNOWER"
    }
})
--[[
rabenhugelFarm:override({
    Id = "CITIZEN",
    CompatibleJobList = {
        Action = "APPEND",
        "RAVH_FARMHAND",
        "RAVH_THRESHERWINNOWER"
    }
})
]]

rabenhugelFarm:log("Job Status Permissions Ammended")