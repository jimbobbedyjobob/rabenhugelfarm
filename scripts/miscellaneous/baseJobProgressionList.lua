local rabenhugelFarm = ...

-- Basic JOB_PROGRESSION Asset, to use until Balancing is possible
--https://www.polymorph.games/foundation/modding/api/job_progression

rabenhugelFarm:register({
        DataType = "JOB_PROGRESSION",
        Id = "BASIC_JOB_PROGRESSION_LIST",

        ProgressionElementList = {
                {XpNeeded = 1},
                {XpNeeded = 2},
                {XpNeeded = 4},
                {XpNeeded = 8},
                {XpNeeded = 16},
                {XpNeeded = 32}
        }
})