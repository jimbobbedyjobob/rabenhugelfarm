local rabenhugelFarm = ...

--[[
COMPONENENT for the pre-attachment of Additional Parts 

]]

--------------------------------------------------------------------------
local COMP_PREATTACH_PART = {
	TypeName = "COMP_PREATTACH_PART",
	ParentType = "COMPONENT",
	Properties = {
		{ Name = "PartToAttach", Type = "string", Default = "" },
		--{ Name = "NodeToWhichItAttaches", Type = "string", Default = "" },
	}
}

--------------------------------------------------------------------------
function COMP_PREATTACH_PART:create()
	self:Attach_Part(partName, "create()")
end

--------------------------------------------------------------------------
function COMP_PREATTACH_PART:init()
	self:Attach_Part("HATCH_BUILDING_PART", "init()") -- how to call internal functions
end

--------------------------------------------------------------------------
function COMP_PREATTACH_PART:Attach_Part(partName, from)
    rabenhugelFarm:log("Comp has run & partName = " .. partName .. " from " .. from)
end

--------------------------------------------------------------------------
rabenhugelFarm:registerClass(COMP_PREATTACH_PART)
