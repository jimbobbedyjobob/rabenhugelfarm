local rabenhugelFarm = ...

-- ammend Vanilla Trade Villages Buying and Selling Reource Lists


-- Override and Append Resource Lists
-- https://www.polymorph.games/foundation/modding/api/trading_village
rabenhugelFarm:override({
    DataType = "TRADING_VILLAGE",
    Id = "VILLAGE_NORTHBURY",

    BuyingResourceList = 
    {
        Action = "APPEND",      
        {   -- https://www.polymorph.games/foundation/modding/api/resource_trading_info
            ResourceMaxAmount = { Resource = "RAVH_BREAD_BARLEY", Quantity = 30},
            ReplenishingAmount = 15,
            UnitPrice = { Resource = "GOLD", Quantity = 8}
        }
    },
    SellingResourceList = 
    {
        Action = "APPEND",
        {   -- https://www.polymorph.games/foundation/modding/api/resource_trading_info
            ResourceMaxAmount = { Resource = "RAVH_SACK_GRAIN_BARLEY", Quantity = 30},
            ReplenishingAmount = 20,
            UnitPrice = { Resource = "GOLD", Quantity = 5}
        }
    }
})

rabenhugelFarm:override({
    DataType = "TRADING_VILLAGE",
    Id = "VILLAGE_MYDDLE",

    BuyingResourceList = 
    {
        Action = "APPEND",      
        {   -- https://www.polymorph.games/foundation/modding/api/resource_trading_info
            ResourceMaxAmount = { Resource = "RAVH_BREAD_RYE", Quantity = 20},
            ReplenishingAmount = 10,
            UnitPrice = { Resource = "GOLD", Quantity = 10}
        }
    }
})

rabenhugelFarm:override({
    DataType = "TRADING_VILLAGE",
    Id = "VILLAGE_DAVENPORT",
    
    UnlockCost = 
    { 
        {
            Resource = "RAVH_BREAD_WHEAT", Quantity = 20
        }
    },
    BuyingResourceList = 
    {
        Action = "APPEND",      
        {   -- https://www.polymorph.games/foundation/modding/api/resource_trading_info
            ResourceMaxAmount = { Resource = "RAVH_BREAD_WHEAT", Quantity = 20},
            ReplenishingAmount = 10,
            UnitPrice = { Resource = "GOLD", Quantity = 12}
        }
    }
})