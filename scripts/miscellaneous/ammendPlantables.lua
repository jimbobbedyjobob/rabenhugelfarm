local rabenhugelFarm = ...

-- New Plantables
rabenhugelFarm:override({
	Id = "DEFAULT_BALANCING",
	PlantableList = {
        Action = "APPEND",
        "RAVH_PLANT_BARLEY",
        "RAVH_PLANT_OAT",
        "RAVH_PLANT_RYE",
        "RAVH_PLANT_WHEAT"
    },
})