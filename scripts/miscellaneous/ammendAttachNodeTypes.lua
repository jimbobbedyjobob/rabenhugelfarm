local rabenhugelFarm = ...

-- Add any new Attach Node Types to the core list

rabenhugelFarm:override({
    Id = "ATTACH_NODE_TYPE",
    {
        Action = "APPEND",
        { "HANGING" }
    }
})

rabenhugelFarm:log("New Attach Node Types Added")