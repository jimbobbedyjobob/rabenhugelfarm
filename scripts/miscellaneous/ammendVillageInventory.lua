local rabenhugelFarm = ...

-- Add Grains to starting inventory
-- Remove Oat Rye and Wheat once they are accessible by Levelling up
rabenhugelFarm:override({
    Id = "DEFAULT_VILLAGE_CENTER_INVENTORY",
    Collection = { 
        --[[
        Action = "APPEND",
        ]]
        {
            Resource = "GOLD",
            Quantity = 100000
        },
        {
            Resource = "WOOD",
            Quantity = 30
        },
        {
            Resource = "BERRIES",
            Quantity = 30
        },
        {
            Resource = "TOOL",
            Quantity = 20
        },
        {
            Resource = "STONE",
            Quantity = 20
        },
        {
            Resource = "WOOLEN_CLOTH",
            Quantity = 15
        }
    }
})