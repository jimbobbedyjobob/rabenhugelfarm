local rabenhugelFarm = foundation.createMod()

-- Register the mods in inverse order of dependency, if that becomes a thing

-- Miscellaneous for all mods
rabenhugelFarm:dofile("scripts/miscellaneous/baseJobProgressionList.lua")
rabenhugelFarm:dofile("scripts/miscellaneous/additionalVillagerNames.lua")
--rabenhugelFarm:dofile("scripts/miscellaneous/registerPreAttachComponent.lua")

-- Not Yet Accsessible
--rabenhugelFarm:dofile("scripts/miscellaneous/ammendNewResourceTypes.lua")
--rabenhugelFarm:dofile("scripts/miscellaneous/ammendAttachNodeTypes.lua")

-- Grain Farm
rabenhugelFarm:dofile("scripts/grainFarm.lua")

-- Adjust Game-Start Village Inventory & Trading
rabenhugelFarm:dofile("scripts/miscellaneous/ammendVillageInventory.lua")
rabenhugelFarm:dofile("scripts/miscellaneous/ammendTradingVillages.lua")

-- Ammend Villager Status Job Permissions
rabenhugelFarm:dofile("scripts/miscellaneous/ammendJobStatusPermissions.lua")

-- Ammend Miscellaneous New Assets
if (foundation.getGameVersion == nil or version.cmp(foundation.getGameVersion(), "1.6") < 0) then

else
    rabenhugelFarm:dofile("scripts/miscellaneous/ammendPlantables.lua")
end

rabenhugelFarm:log("Rabenhugel Farm Mod Registry Complete")